
function MiniQuestController(game) {
	var _this = this;

	this.init = function() {
		this.game = game;

		quests = [
			BasicVariables, NewVariable,
			NewInteger, NewFloat,
			AddVariables, SubtractVariables, MultiplyVariables, DivideVariables,
			BasicIf, IfElse, BasicEquals, BasicSmallerThan, BasicGreaterThan,
			BasicWhile,
			BaicLists, ListPush, ListSplice, ListWhile,
			FunctionBasic, FunctionParameters, FunctionReturn
		]
		this.questList = [];

		for (var i in quests) {
			var quest = new quests[i](this.game);
			this.questList.push(quest);
		}

		$("#scrollButton").click(function() {
			_this.createMainElement();

			$("#spellBook").html(_this.mainElement);
			$("#spellBookContainer").show();

			$(".lostPageName").click(function(event) {
				_this.game.displayQuest($(this).parent().data("Quest"), 1);
			});

			$(".lostPageQuest").click(function(event) {
				_this.game.displayQuest($(this).parent().data("Quest"), 2);
			});
		});
	}

	this.createMainElement = function() {
		this.mainElement = $("<div id='lostPages'><h3>Kadonneet sivut</h3></div>");

		for (var i in this.questList) {
			var quest = this.questList[i];

			var questElement = $("<div class='lostPage'></div>");

			if (quest.found == true)
				var questName = $("<div class='lostPageName'>" + quest.questTitle + "</div>");
			else
				var questName = $("<div class='lostPageNameNotFound'>" + quest.questTitle + "</div>");

			questElement.data("Quest", quest);

			this.mainElement.append(questElement);

			if (quest.found == false) {
				this.game.bindHover(questElement, "Et ole vielä löytänyt tätä sivua!");
				questElement.addClass("lostPageNotFound");

				questElement.append(questName);
			}
			else {
				var questDescription = $("<div class='lostPageQuest'>Tehtävä: " + quest.shortDescription + "</div>");

				this.game.bindHover(questName, "Näytä taikaan liittyvää tietoa");
				this.game.bindHover(questDescription, "Näytä tehtävä");

				questElement.append(questName, questDescription);

				if (quest.questFinished == true) {
					var completeSign = $("<div class='lostPageQuestStatus lostPageComplete'>✓</div>")
					questElement.append(completeSign);

					this.game.bindHover(completeSign, "Olet päässyt tehtävän läpi!");
				}
				else {
					var completeSign = $("<div class='lostPageQuestStatus lostPageIncomplete'>✗</div>")
					questElement.append(completeSign);

					this.game.bindHover(completeSign, "Et ole vielä läpäissyt tätä tehtävää")
				}
			}	
		}
	}

	// Activate quests that are on the given level
	this.levelCheck = function(levelCounter) {
		for (var i in this.questList) {
			var quest = this.questList[i];
			
			if (levelCounter == quest.activeLevel)
				quest.activate();
		}
	}

	// Check if there are undone quests on the given level
	this.levelScrollsUndone = function(levelCounter) {
		for (var i in this.questList) {
			var quest = this.questList[i];

			if (quest.activeLevel == levelCounter && quest.questActivated == false &&
				quest.questFinished == false)
				return true;
		}

		return false;
	}

	this.pickLevelScrolls = function(levelCounter) {
		for (var i in this.questList) {
			var quest = this.questList[i];

			if (quest.activeLevel == levelCounter && quest.questActivated == false &&
				quest.questFinished == false)
				quest.playerWalk(undefined, true);
		}
	}
	
	this.init();
}

function MiniQuest(game) {
	var _this = this;

	this.init = function() {
		this.game = game;
		this.context = game.mapContext;
		this.level = this; // Need this because of bad way to eval code
		
		this.questActivated = false;
		this.questFinished = false;
		this.found = false;
		
		this.editedCode = "";

		this.questElement = $("<div><p>" + this.questDescription + "</p><div class='codeDiv'><textarea>\n" + this.editorCode + "</textarea></div></div>");

		this.pagination = true;
		this.pages = {1: this.theoryPage, 2: this.questElement}
	}

	// Player grabs the scroll
	this.playerWalk = function(player, silent) {
		this.found = true;
		this.questActivated = true;

		this.game.animationsController.removeAnimation(_this.sprite);
		this.game.removeLevelObject(this);

		if (silent == undefined || silent == false) {
			$("#scrollButton").hide("explode").show("explode");

			this.game.createSpeech(this.game.player, "Löysin kadonneen sivun, jonka otsikkona on <b>" +
				this.questTitle + "</b>!");

			this.game.displayQuest(this);
		}
		return true;
	}

	this.runCode = function(code, quest, theoryCode) {

		// Run the staging code
		if (theoryCode == true)
			eval(this.theoryStagingCode);
		else
			eval(this.stagingCode);

		// Run the actual editor code
		var errorFree = true;
		try {
			eval(code);
		}
		catch (e) {
			errorFree = false;
			this.game.showCodeError(e.lineNumber, e.message);
		}

		// Run the finalizing code
		if (errorFree == false)
			return;

		if (theoryCode == true)
			eval(this.theoryFinalizingCode);
		else
			eval(this.finalizingCode);
			
	}
	
	this.activate = function() {	
		this.game.level.levelObjects.push(this);
		
		this.sprite = this.game.animationsController.newAnimation({
			src: "scroll.png",
			width: 32,
			height: 32,
			frames: 1,
			frameWidth: 32,
			frameHeight: 32,
			animationLine: 0,
			x: this.x * 32,
			y: this.y * 32
		});
	}

	this.finishQuest = function() {
		this.game.displayFinishQuest(this);
		this.questFinished = true;
	}

	this.init();
}

MiniQuest.prototype.stagingCode = function() {}
MiniQuest.prototype.finalizingCode = function() {}

MiniQuest.prototype.theoryStagingCode = function() {}
MiniQuest.prototype.theoryFinalizingCode = function() {}


function BasicVariables(game) {
	var _this = this;

	this.init = function() {
		this.x = 6;
		this.y = 30;
		this.activeLevel = 1;
		
		this.questTitle = "Muuttujat";
		this.shortDescription = "Muuttujan arvon muuttaminen"
		this.questDescription = hereDoc(function() {/*!
<div>
Alla oleva muuttuja väittää, että kuvaamataito ja liikunta olisivat luonnontieteitä. Voisitko laittaa
muuttujan arvoksi sen sijaan <b>fysiikka ja kemia</b>?
</div>
*/});

		this.stagingCode = hereDoc(function() {/*!
			var luonnontieteita;
*/});

		this.editorCode = hereDoc(function() {/*!
luonnontieteita = "kuvis ja liikunta" // Muuttuja nimeltään <b>luonnontieteitä</b>, jonka arvo on "kuvis ja liikunta"
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (luonnontieteita == undefined)
				sano("Laita muuttuja nimeltään luonnontieteita koodiin!");
			else if (luonnontieteita != "fysiikka ja kemia") {
				sano("Muuttujan luonnontieteita arvo on " + luonnontieteita + ". Se ei ole oikea vastaus!");
			}
			else {
				sano("Näyttää paremmalta. Muuttujan arvo on nyt <b>fysiikka ja kemia</b>.")
				this.finishQuest();
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var kamppailulaji;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Muuttujat</h3>

<div>
Ohjelmoinnissa käytetään usein asioita nimeltään <b>muuttujat</b>. Niihin voit laittaa mitä tahansa tietoa,
kuten jonkun nimi, ikä tai hänen katselemiensa videoiden lukumäärän Youtubesta.
<br /><br />
Muuttujilla on aina jokin <b>nimi</b> ja <b>arvo</b>. Alla näet muuttujan, jonka nimi on <b>kamppailulaji</b>
ja sen arvo on <b>"Kung-fu"</b>.
<br /><br />
Kokeile koodia aluksi ja koita sitten muuttaa muuttujan arvoa.
</div>

<div class="codeDiv">
<textarea class="originalCode">

kamppailulaji = "Kung-fu" // Tässä <b>kamppailulaji</b>-muuttujan arvo on "Kung-fu". Voit yrittää muuttaa arvoa!
</textarea>
</div>

</div>
*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (kamppailulaji == undefined)
				sano("Muuttujaa kamppailulaji ei enää ole!");
			else
				sano("Osaan arvon herrat sellaista lajia kuin " + kamppailulaji + "!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BasicVariables.prototype = Object.create(MiniQuest.prototype);
BasicVariables.prototype.constructor = BasicVariables;


function NewVariable(game) {
	var _this = this;

	this.init = function() {
		this.x = 7;
		this.y = 20;
		this.activeLevel = 1;
		
		this.questTitle = "Uudet muuttujat";
		this.shortDescription = "Tee uusi muuttuja";
		this.questDescription = hereDoc(function() {/*!
<div>
Nyt kun suunnilleen tiedämme mitä muuttujat ovat, osaatko tehdä alle uuden muuttujan, jonka nimi on
<b>kivatyyppi</b> ja sen arvoksi laittaa oman nimesi?
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var kivatyyppi;
*/});

		this.editorCode = hereDoc(function() {/*!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (kivatyyppi == undefined)
				sano("Yritä tehdä <b>kivatyyppi</b>-niminen muuttuja ja anna sille arvoksi oman nimesi!");
			else {
				sano("Eräs kiva tyyppi on nimeltään " + kivatyyppi + "!");
				this.finishQuest();
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var pelikonsoli, lempipeli;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Uudet muuttujat</h3>

<div>
Muuttujia voidaan tehdä mielin määrin uusia ja niiden arvoa voidaan muuttaa miten ikinä haluamme.
<br /><br />
Alla näet muuttujan, jonka nimi on <b>pelikonsoli</b> ja sen arvo on <b>"Playstation 4"</b>. Osaatko
tehdä toisen muuttujan nimeltä lempipeli ja antaa sille arvoksi jonkin pelin nimen?
<br /><br />
</div>

<div class="codeDiv">
<textarea class="originalCode">
pelikonsoli = "Playstation 4" // Tässä pelikonsoli-muuttujan arvo on "Playstation 4". Voit yrittää muuttaa arvoa!
</textarea>
</div>

</div>
*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (pelikonsoli == undefined)
				sano("Muuttujaa pelikonsoli ei enää ole!");
			else
				sano("Muuttujan pelikonsoli arvo on " + pelikonsoli);

			if (lempipeli != undefined)
				sano("Lempipelini on " + lempipeli);
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

NewVariable.prototype = Object.create(MiniQuest.prototype);
NewVariable.prototype.constructor = NewVariable;


function NewInteger(game) {
	var _this = this;

	this.init = function() {
		this.x = 18;
		this.y = 32;
		this.activeLevel = 2;
		
		this.questTitle = "Luvut muuttujina";
		this.shortDescription = "Tee uusi lukumuuttuja";
		this.questDescription = hereDoc(function() {/*!
<div>
Sama harjoitus kuin aiemminkin, alokas!
<br /><br />
Tee muuttuja <b>peliarvostelu</b> ja anna sille arvoksi jokin numero! Jos haluat, tee lisäksi jälleen
muuttuja nimeltään lempipeli ja anna sille mikä tahansa arvo (numero tai ei).
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var peliarvostelu, lempipeli;
*/});

		this.editorCode = hereDoc(function() {/*!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (peliarvostelu == undefined)
				sano("Muuttujaa <b>peliarvostelu</b> ei ole koodissa!");
			else if (typeof peliarvostelu != "number")
				sano("Laita muuttujan <b>peliarvostelu</b> arvoksi jokin numero!");
			else {
				if (lempipeli != undefined) {

					if (peliarvostelu > 6)
						sano("Pelin " + lempipeli + " arvostelu on " + peliarvostelu + ", eli melko hyvä!");
					
					else if (peliarvostelu > 4)
						sano("Peli " + lempipeli + " sai keskinkertaisen arvostelun, eli numeron " + peliarvostelu);
					
					else
						sano("Kuka kehtasi antaa pelille " + lempipeli + " noin huonon arvostelun, eli numeron " + peliarvostelu + "!");
					
				}
				else {
					if (peliarvostelu > 6)
						sano("Pelin arvostelu on " + peliarvostelu + ", eli melko hyvä!");
					
					else if (peliarvostelu > 4)
						sano("Peli sai keskinkertaisen arvostelun, eli numeron " + peliarvostelu);
					
					else
						sano("Peli sai huonon arvostelun, eli numeron " + peliarvostelu);
				}
				this.finishQuest()
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var donitseja, possumunkkeja;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Luvut muuttujina</h3>

<div>
Aiemmat esimerkit muuttujista oli tehty niin, että muuttujien arvo oli "lainausmerkkien" sisällä. Se
tarkoitti sitä, että ne olivat kirjainmuuttujia.
<br /><br />
Muuttujien arvona voi olla myös pelkkä numero, jolloin lainausmerkkejä ei tarvitse käyttää!
<br /><br />
Alla olevassa koodissa oleva muuttuja kertoo donitsien määrän. Voit yrittää tehdä toisenkin muuttujan,
jonka nimi on <b>possumunkkeja</b> ja antaa sille jonkin numeroarvon.
</div>

<div class="codeDiv">
<textarea class="originalCode">
donitseja = 12 // Donitseja on 12 kappaletta, eli täysi tusina! (Ei kauaa, omnomnom...)
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (donitseja == undefined)
				sano("Muuttujaa <b>donitseja</b> ei enää ole!");
			else
				sano("Meillä on donitseja mukana kokonaiset " + donitseja);

			if (possumunkkeja != undefined)
				sano("Muahahaa, meillä on aseenamme possumunkkeja " + possumunkkeja + " kappaletta!")
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

NewInteger.prototype = Object.create(MiniQuest.prototype);
NewInteger.prototype.constructor = NewInteger;


function NewFloat(game) {
	var _this = this;

	this.init = function() {
		this.x = 9;
		this.y = 19;
		this.activeLevel = 2;
		
		this.questTitle = "Desimaaliluvut";
		this.shortDescription = "Anna muuttujalle desimaaliarvo";
		this.questDescription = hereDoc(function() {/*!
<div>
Mikäs sen parempi tapa kikkailla desimaalien kanssa kuin rahan räknäys! Tee alle muuttujat nimeltään
<b>rahaaTililla</b> ja <b>rahaaLompakossa</b>. Anna niille vielä jotkin desimaaliarvot!
<br /><br />
Halajat varmaan tietää miten muuttujat voidaan laskea yhteen. Tästä kerron kohta (tai voit koittaa arvata)!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var rahaaTililla, rahaaLompakossa;
*/});

		this.editorCode = hereDoc(function() {/*!

*/});

		this.finalizingCode = hereDoc(function() {/*!
			var right = 0;
			var allExist = true;

			if (rahaaTililla == undefined) {
				sano("Muuttujaa <b>rahaaTililla</b> ei ole!");
				allExist = false;
			}

			if (rahaaLompakossa == undefined) {
				sano("Muuttujaa <b>rahaaLompakossa</b> ei ole!");
				allExist = false;
			}

			if (allExist == true) {
				if (typeof rahaaTililla != "number" || rahaaTililla % 1 === 0)
					sano("Muuttujan rahaaTililla pitää olla desimaali!")
				else
					right += 1;

				if (typeof rahaaLompakossa != "number" || rahaaLompakossa % 1 === 0)
					sano("Muuttujan rahaaLompakossa pitää olla desimaali!")
				else
					right += 1;

				if (right == 2) {
					var total = (rahaaLompakossa + rahaaTililla).toPrecision(3);
					sano("Meillä on rahaa lompakossa " + rahaaLompakossa + ", ja tilillä taas " + rahaaTililla);
					sano("Yhteensä rahaa on " + total + ". Olen rikas!");
					this.finishQuest();
				}
			}

*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var donitsit, donitsinHinta;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Desimaaliluvut</h3>

<div>
Okei, tasaluvuista päästään desimaalilukuihin, jotka nekin voivat olla muuttujien arvoina.
<br /><br />
Mitä ihmeellistä desimaaliluvuin varustetuissa muuttujissa sitten on? No ei kummempaa, kuin että
muista käyttää <b>pistettä pilkun sijaan</b>, kun kirjoitat desimaalin!
<br /><br />
Katsos alla olevaa koodia, jossa meillä onkin tällä kertaa desimaalimäärä donitseja! (Kuka kehtasi
vetää puolet munkista?!) Koita tehdä myös muuttuja <b>donitsinHinta</b> ja anna sille jokin arvo.
</div>

<div class="codeDiv">
<textarea class="originalCode">
donitsit = 10.5 // Donitseja on 10,5 kappaletta. Käytä koodissa <b>pilkun sijasta pilkkua</b>!

</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (donitsit == undefined)
				sano("Muuttujaa <b>donitsit</b> ei ole!");
			else
				sano("Donitseja on nyt " + donitsit + " kipaletta");

			if (donitsinHinta != undefined)
				sano("Donitsin hinta on huimat " + donitsinHinta + "! Taitaa poliiseja pyörryttää...");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

NewFloat.prototype = Object.create(MiniQuest.prototype);
NewFloat.prototype.constructor = NewFloat;


function AddVariables(game) {
	var _this = this;

	this.init = function() {
		this.x = 4;
		this.y = 25;
		this.activeLevel = 3;
		
		this.questTitle = "Muuttujien laskeminen yhteen";
		this.shortDescription = "Laske muuttujat yhteen";
		this.questDescription = hereDoc(function() {/*!
<div>
Aiemmassa tehtävässä teimme tilin ja lompakon rahan määrää kuvaavat muuttujat. Lasketaan tällä kertaa
rahat yhteen!
<br /><br />
Tee alle lisäksi muuttuja <b>rahaaLompakossa</b> ja anna sille jokin arvo. Tee vielä toinen muuttuja
<b>rahaaYhteensa</b> ja laita sen arvoksi muuttujien rahaaTililla ja rahaaLompakossa arvot!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var rahaaTililla, rahaaLompakossa, rahaaYhteensa;
*/});

		this.editorCode = hereDoc(function() {/*!
rahaaTililla = 30.5 // Tilillä on rahaa 30,5 euroa!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			var pass = true;

			if (rahaaTililla == undefined) {
				sano("Meillä ei ole muuttujaa <b>rahaaTililla</b>!");
				pass = false;
			}

			else if (rahaaLompakossa == undefined) {
				sano("Meillä ei ole muuttujaa <b>rahaaLompakossa</b>!");
				pass = false;
			}

			else if (rahaaYhteensa == undefined) {
				sano("Meillä on muut tarvittavat muuttujat paitsi <b>rahaaYhteensa</b>!");
				pass = false;
			}

			if (pass == true) {
				sano("Tilillä rahaa " +  rahaaTililla + " euroa, ja lompakossa " + rahaaLompakossa + " euroa!");
				sano("Laskin, että rahaa on yhteensä " + rahaaYhteensa + " euroa.");

				if (rahaaYhteensa == rahaaTililla + rahaaLompakossa) {
					sano("Ja summahan näyttäisi olevan oikein! Tehtävä suoritettu!");
					this.finishQuest();
				}
				else {
					sano("Summa ei täsmää. En tainnut laskea rahoja oikein yhteen...");
				}
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var oppilaita, fysiikanOpet, matikanOpet, opejaYhteensa;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Muuttujien yhteenlaskeminen</h3>

<div>
Toivon mukaan muistat, että muuttujat voivat koostua kirjaimista ja numeroista (tasa- ja 
desimaaliluvut). Olet ehkä miettinyt miten muuttujia voitaisiin laskea yhteen - tässä näytetään miten
se tapahtuu!
<br /><br />
Voit laskea muuttujia keskenään yhteen tai lisätä niihin suoraan numeroita. Katsohan alle! Osaatko lisätä
opejaYhteensa-muuttujaan vielä jonkin luvun koskematta riviin 6?
</div>

<div class="codeDiv">
<textarea class="originalCode">
oppilaita = 20 + 24 // Lasketaan kahden luokan oppilaat yhteen, joilla sattuu olemaan 20 ja 24 oppilasta!

fysiikanOpet = 2 // Fysiikan opettajia on 2
matikanOpet = 3 // Matikan opettajia on 3

opejaYhteensa = fysiikanOpet + matikanOpet // Lasketaan yhteen fysiikan ja matikan opettajat (plus yksi tukiope)!

</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (oppilaita != undefined)
				sano("Koulun A- ja B-luokilla on oppilaita " + oppilaita);

			if (fysiikanOpet != undefined && matikanOpet != undefined && opejaYhteensa != undefined)
				sano("Fysiikan ja matikan opeja on yhteensä " + opejaYhteensa);
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

AddVariables.prototype = Object.create(MiniQuest.prototype);
AddVariables.prototype.constructor = AddVariables;


function SubtractVariables(game) {
	var _this = this;

	this.init = function() {
		this.x = 7;
		this.y = 17;
		this.activeLevel = 3;
		
		this.questTitle = "Muuttujien vähentäminen";
		this.shortDescription = "Vähennä muuttujien avulla";
		this.questDescription = hereDoc(function() {/*!
<div>
Kuvitellaanpas, että olet saanut käsiisi laatikollisen suklaapusuja. Et kuitenkaan pidä vaniljapusuista,
joten otat ne pois ja syötät ne hirvitykset muille.
<br /><br />
Alla on muuttuja <b>suklaapusuja</b>, jonka arvo on 20,2 (kyllä, joku söi osan pususta). Tee muuttuja
<b>vaniljapusuja</b>, ja anna sille jokin arvo. Tee vielä muuttuja <b>hyviaPusuja</b>, jonka arvoksi
tulee suklaapusujen ja vaniljapusujen vähennys.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var suklaapusuja, vaniljapusuja, hyviaPusuja;
*/});

		this.editorCode = hereDoc(function() {/*!
suklaapusuja = 20.2
*/});

		this.finalizingCode = hereDoc(function() {/*!

			sano("Suklaapusuja on " + suklaapusuja + ". Vaniljapusuja taas on " + vaniljapusuja + " (hyh...)");
			sano("Hyviä pusuja on yhteensä " + hyviaPusuja);

			if (hyviaPusuja == suklaapusuja - vaniljapusuja) {
				sano("Lasku näyttäisi olevan oikein!");
				this.finishQuest();
			}
			else {
				sano("Lasku ei mennyt oikein...");

				if (suklaapusuja == undefined)
					sano("Muuttujaa suklaapusuja ei ole!");
				
				if (vaniljapusuja == undefined)
					sano("Muuttujaa vaniljapusuja ei ole!");
				
				if (hyviaPusuja == undefined)
					sano("Muuttujaa hyviaPusuja ei ole!");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var lampotilaUlkona, lampotilaSisalla, laskeLampotilaa;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Muuttujien vähentäminen</h3>

<div>
Aivan kuten muuttujien avulla voidaan plussailla, luonnistuvat myös vähennyslaskut!
<br /><br />
Homma toimii melko samalla tavalla, kuin muulloinkin, mutta plusmerkki on nyt vain miinusmerkki! Osaatko
ottaa alla olevasta koodista sisälämpötilasta vielä pois 5 astetta? (Tai ehkä lisätä, muahaha!)
</div>

<div class="codeDiv">
<textarea class="originalCode">
lampotilaUlkona = 35 // Lämpötila ulkona on 35 (huhhuh...)
lampotilaUlkona = lampotilaUlkona - 15 // Otetaan lämpötilasta vähän pois, niin on vähän siedettävämpi :)

lampotilaSisalla = 42 // Sisällä on vielä kuumempi meno!
laskeLampotilaa = 18 // Haluamme laskea lämpöä näin paljon

lampotilaSisalla = lampotilaSisalla - laskeLampotilaa // Otetaan sisälämpötilasta halutun verran pois!
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (lampotilaUlkona < 25 && lampotilaUlkona > 15)
				sano("Lämpötila ulkona on ihanaiset " + lampotilaUlkona + " astetta :)");
			else
				sano("Nyt ulkona on " + lampotilaUlkona + " astetta.");

			if (lampotilaSisalla < 25 && lampotilaSisalla > 15)
				sano("Sisällä on mukavat " + lampotilaSisalla + " astetta lämmintä!");
			else
				sano("Sisällä on " + lampotilaSisalla + " astetta.");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

SubtractVariables.prototype = Object.create(MiniQuest.prototype);
SubtractVariables.prototype.constructor = SubtractVariables;


function MultiplyVariables(game) {
	var _this = this;

	this.init = function() {
		this.x = 4;
		this.y = 12;
		this.activeLevel = 3;
		
		this.questTitle = "Muuttujien kertolaskut";
		this.shortDescription = "Kerro muuttujia keskenään";
		this.questDescription = hereDoc(function() {/*!
<div>
Tässä tehtävässä lasketaan kuinka monta mokkapalaa tarvitaan övereihin synttärijuhliimme.
<br /><br />
Tee muuttuja <b>vieraita</b> ja anna sille arvoksi 12. Tee toinen muuttuja <b>palojaPerVieras</b>, jonka
arvoksi laitat arviosi siitä, kuinka monta mokkapalaa kukin vieras mättää naamaansa.
<br /><br />
Tee lopuksi muuttuja <b>mokkapalojaYhteensa</b>, jonka arvoksi tulee aiempien muuttujien kertolasku.
Teet vielä varmuuden vuoksi hieman lisää mokkapaloja, joten kerro mokkaPalojaYhteensa vielä luvulla 1,5!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var vieraita, palojaPerVieras, mokkapalojaYhteensa;
*/});

		this.editorCode = hereDoc(function() {/*!
*/});

		this.finalizingCode = hereDoc(function() {/*!

			if (vieraita == undefined)
				sano("Kannattaa aloittaa laittamalla muuttuja <b>vieraita</b> koodiin!");

			else if (vieraita != 12)
				sano("Vieraita on tulossa 12, ei " + vieraita + "!");

			else if (mokkapalojaYhteensa == vieraita * palojaPerVieras * 1.5) {
				sano("Mokkapalojen määrä on juuri oikein, eli " + mokkapalojaYhteensa)
				this.finishQuest();
			}

			else {
				sano("Mokkapalojen määrä, eli " + mokkapalojaYhteensa + ", ei ole aivan oikein...");

				if (palojaPerVieras == undefined)
					sano("Muuttujaa <b>palojaPerVieras</b> ei ole!");

				else if (mokkapalojaYhteensa == undefined)
					sano("Muuttujaa <b>mokkapalojaYhteensa</b> ei ole!");

				else
					sano("Ehkä et muistanut kertoa mokkapalojen määrää lopuksi luvulla 1,5?");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var viikkoraha, viikkoja, rahaaYhteensa;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Muuttujien kertolaskut</h3>

<div>
Muuttujien kertolaskut ovat myös mahdollisia. Niissä käytetään tähtimerkkiä, eli *
<br /><br />
Katsohan alla olevaa koodia, jossa lasketaan muutaman viikon viikkorahat. Koska käyttäydyit niin hienosti,
kerro rahan yhteismäärä vielä jollakin sopivalla luvulla (desimaaliluku sopisi kivasti, uuu...)!
</div>

<div class="codeDiv">
<textarea class="originalCode">
viikkoraha = 15 // Viikkoraha on 15 euroa
viikkoja = 4 // Viikkoja kuluu 4

rahaaYhteensa = viikkoraha * viikkoja // Lasketaan neljän viikon viikkorahat. Koita vielä tämä vielä jollakin (desimaali)luvulla.
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			sano("Koska saan viikkorahaa " + viikkoraha + ", ja viikkoja on jo kulunut " + viikkoja + "...");
			sano("Olen selvästi oikeutettu saamaan yhteensä " + rahaaYhteensa + " euroa!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

MultiplyVariables.prototype = Object.create(MiniQuest.prototype);
MultiplyVariables.prototype.constructor = MultiplyVariables;


function DivideVariables(game) {
	var _this = this;

	this.init = function() {
		this.x = 17;
		this.y = 14;
		this.activeLevel = 3;
		
		this.questTitle = "Muuttujien jakolaskut";
		this.shortDescription = "Jaa muuttujia keskenään";
		this.questDescription = hereDoc(function() {/*!
<div>
Sinulta sattui löytymään muhkea karkkipussi taskusi pohjalta. Kaverisi kiertelevät kuin hait ympärilläsi
ja pääset jakamaan karkit heidän kanssaan. Haluat kuitenkin jakaa vain puolet karkeistasi heille!
<br /><br />
Tee muuttuja <b>karkkeja</b> ja anna sille jokin numeroarvo (desimaalikin on ihan hyvä).
<br /><br />
Tee toinenkin muuttuja nimeltään <b>kavereita</b>, jonka arvo on 4. Tee vielä kolmas muuttuja
<b>karkkejaKullekin</b>, jonka arvo on karkkien yhteismäärä jaettuna kahdella ja kavereiden määrällä!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var karkkeja, kavereita, karkkejaKullekin;
*/});

		this.editorCode = hereDoc(function() {/*!

*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (kavereita == undefined)
				sano("Laita koodiin muuttuja <b>kavereita</b>!");

			else if (kavereita != 4)
				sano("Jaat karkkejasi neljälle kaverellisi, et " + kavereita + " kaverille!");

			else if (karkkejaKullekin == karkkeja / 2 / kavereita) {
				sano("Annat kullekin kaverille " + karkkejaKullekin + " karkkia!");
				this.finishQuest();
			}
			else {
				if (karkkeja == undefined)
					sano("Laita muuttuja <b>karkkeja</b> koodiin!");

				else if (karkkejaKullekin == undefined)
					sano("Laita muuttuja <b>karkkejaKullekin</b> koodiin!");

				else
					sano("Koitat antaa väärän määrän karkkeja, eli " + karkkejaKullekin + ". Jaoitko karkkien kokonaismäärän kahdella?");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var pizzoja, syojia, syomista;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Muuttujien jakolaskut</h3>

<div>
Viimeinen muuttujien laskutoimitus on jakolaskut. Tämä toimitus toimii samaan tapaan kuin aiemmatkin,
ja tässä jakomerkkinä toimii kenoviiva, eli /
<br ><br />
Koska pizzalähetti kaatui pyörällä matkallaan ja puolet pizzoista putosi rotkoon, koita jakaa pizzojen
määrä vielä kahdella!
</div>

<div class="codeDiv">
<textarea class="originalCode">
pizzoja = 7.5 // Hmmm... pizzalähetti toi puolikkaan pizzan seitsemän muun lisäksi...
syojia = 4 + 1 // Syöjiä sattuu olemaan neljä ihmistä (plus yksi alieni)

var syomista = pizzoja / syojia // Lasketaan kuinka monta pizzaa kukin syöjä saa
</textarea>
</div>

</div>
*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			sano("Meille tuli pizzoja " + pizzoja + "." + " Koska syöjiä on " + syojia + ", niin syömistä " + syomista + " on pizzaa kullekin!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

DivideVariables.prototype = Object.create(MiniQuest.prototype);
DivideVariables.prototype.constructor = DivideVariables;


function BasicIf(game) {
	var _this = this;

	this.init = function() {
		this.x = 5;
		this.y = 30;
		this.activeLevel = 4;
		
		this.questTitle = "If-taikasana";
		this.shortDescription = "Päätä ostatko jäätelöä";
		this.questDescription = hereDoc(function() {/*!
<div>
Sattuu olemaan kiva päivä ja haluat ehkä ostaa jäätelöä. Mietit kuitenkin, että ostat jäätelöä vain,
jos lämpötila on sopiva.
<br /><br />
Osaatko laittaa if-taikasanan sulkeiden sisään muuttujan <b>ostos</b>, jolle annat arvoksi jonkin
lempijäätelösi nimen? Muuta vielä lämpötilaa niin, että saat ostettua jäätelön!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var lampotila;
			var ostos = "";
*/});

		this.editorCode = hereDoc(function() {/*!
lampotila = 19 // Lämpötila on nyt 19 astetta

if (lampotila > 20) // Jos lämpötila on yli 20 astetta...
{ // Kaarisulje, joka tarvitaan if-taikasanan eteen
		// Tähän tulee muuttuja, joka kertoo ostoksesi!
} // Toinen kaarisulje, joka myös tarvitaan if-taikasanan jälkeen
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (lampotila == undefined)
				sano("Laita koodiin muuttuja <b>lampotila</b> ja käytä sitä if-taikasanassa!");

			else if (ostos == undefined)
				sano("Laita <b>if-taikasanan</b> sisään muuttuja <b>ostos</b>!");

			else if (typeof ostos != "string")
				sano("Laita muuttujan <b>ostos</b> arvoksi lempijäätelösi nimi!");

			else if (ostos.length == 0)
				sano("Laitoitko <b>ostos</b>-muuttujaa if-taikasanan sulkeisiin tai muistitko säätää lämpötilan?");

			else {
				sano("Mm-mm, " + ostos + " on suurinta herkkua tällaisilla " + lampotila + " asteen lämmöillä!");
				this.finishQuest();
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var rahaaLompakossa, ostettavaLevy, parasKappale;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>If-taikasana</h3>

<div>
Oletko koskaan tehnyt seuraavanlaista pohdintaa?
<br ><br />
<i>"Jos lompakossa on rahaa yli 20 euroa, ostan Daruden levyn."</i>
<br ><br />
No et ole välttämättä ajatellut Daruden levyä, mutta olet varmaan miettinyt onko sinulla yleensäkään rahaa
ostaa jotakin mitä haluaisit!
<br ><br />
Kun halutaan koodata edeltävän kaltainen kaltainen päätös, voidaan käyttää <b>if-taikasanaa</b>. Alla näet
esimerkin sen käytöstä.
<br ><br />
Kokeile koodia ensin ja koita sitten muuttaa rahan määrää. Yritä vielä laittaa ostettavan levyn lisäksi
kaarisulkeiden väliin <b>parasKappale</b>-muuttuja, jolle annat haluamasi kappaleen nimen!
</div>


<div class="codeDiv">
<textarea class="originalCode">
rahaaLompakossa = 21 // Lompakossa olevan rahan määrä

if (rahaaLompakossa > 20) // If-taikasana, jolla katsotaan onko lompakossa rahaa enemmän kuin 20 (> tarkoittaa "enemmän kuin")
{ // Kaarisulkeeksi kutsuttu häkkyrä, joka laitetaan heti if-taikasanan jälkeen
	ostettavaLevy = "Darude - Da Sandstorm" // Jos rahaa on tarpeeksi, osta levy
} // Toinen kaarisulje, joka laitetaan myös if-taikasanan sisältämän koodin jälkeen!
</textarea>
</div>

</div>
*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (ostettavaLevy == undefined)
				sano("Byhyy, en voikaan ostaa levyä :(");
			else {
				sano("Jee! Aion ostaa levyn " + ostettavaLevy + "!");

				if (parasKappale != undefined)
					sano("Levyn paras kappale on " + parasKappale + "!");
			}
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BasicIf.prototype = Object.create(MiniQuest.prototype);
BasicIf.prototype.constructor = BasicIf;


function IfElse(game) {
	var _this = this;

	this.init = function() {
		this.x = 4;
		this.y = 24;
		this.activeLevel = 4;
		
		this.questTitle = "Else if -taikasana";
		this.shortDescription = "Mitä tehdään välitunnilla?";
		this.questDescription = hereDoc(function() {/*!
<div>
Osaatko kirjoittaa <b>else if</b> ja <b>else</b>-taikasanat alla olevan if-taikasanan seuraksi?
<br ><br />
Kirjoita taikasanat niin, että jos akkua on jäljellä yli 20, niin välitunnilla selataan
nettiä. Muussa tapauksessa välitunnilla ladataan akkua!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var valitunti;
*/});

		this.editorCode = hereDoc(function() {/*!
akkuaJaljella = 60

if (akkuaJaljella > 50) // Onko akkua jäljellä enemmän kuin 20?
{ // Muista kaarisulkeet alkuun (Myös else if ja else -taikasanoihin!)
	valitunti = "pelaa Flappy Birdsiä!" // Jos akkua on yli 20, pelataan välitunnilla Flappy Birdsiä!
} // Muista kaarisulkeet myös loppuun!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			var originalBattery = akkuaJaljella;
			var originalActivity = valitunti;

			code = code.replace("akkuaJaljella", "x");
			activities = [];
			
			akkuaJaljella = 51;
			valitunti = undefined;
			eval(code);
			activities.push(valitunti);

			akkuaJaljella = 21;
			valitunti = undefined;
			eval(code);
			activities.push(valitunti);

			akkuaJaljella = 19;
			valitunti = undefined;
			eval(code);
			activities.push(valitunti);

			var pass = true;

			if (activities.indexOf(undefined) != -1)
				pass = false;

			// Check for duplicates in the activities array
			for (var i = activities.length - 1; i >= 0; i -= 1){
				var act = activities.splice(i, 1)[0];

				if (activities.indexOf(act) != -1) {
					pass = false;
					break;
				}
			}

			sano("Akkua on jäljellä " + originalBattery + ", joten välitunnin aktiviteetti on: " + originalActivity);

			if (pass == true) {
				sano("Sait aikaan kaivatut else if ja else-taikasanat")
				this.finishQuest();
			}
			else
				sano("Jossakin on vielä vikaa... laitoitko <b>else if</b> (akkua jäljellä yli 50) ja <b>else</b>-taikasanan koodiin?")
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var rahaaLompakossa;
			var ostos = "ei mitään";
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Else if -taikasana</h3>

<div>
Kun aiemmin pohdimme rahaa lompakossa ja sitä, voimmeko ostaa Daruden levyn, emme miettineet muita
vaihtoehtoja. Sen lisäksi olisimme voineet ostaa vaikka uuden tietokonepelin, jos rahaa olisi ollut
yli 40 euroa!
<br /><br />
Kokeile alla olevaa koodia ensin ja koita sitten muuttaa rahan määrää ja kokeile mitä tapahtuu!
<br /><br />
Kokeile myös lisätä yksi <b>else if</b> -taikasana alla olevaan koodiin. Laita se nykyisellään olevan
else if -taikasanan jälkeen tai eteen.
<br /><br />
Mitä tapahtuu jos otat else-taikasanan sulkeineen päivineen pois ja laitat rahan määräksi pienen luvun?
</div>

<div class="codeDiv">
<textarea class="originalCode">
rahaaLompakossa = 27 // Lompakossa olevan rahan määrä

if (rahaaLompakossa > 40) // Jos rahaa on lompakossa enemmän kuin 40
{ // If-taikasanan vaatima kaarisulje alkuun
	ostos = "Vihaiset linnut - sikojen valtakunta" // Jos rahaa on yli 40 euroa, osta peli
} // If-taikasanan päättävä kaarisulje

else if (rahaaLompakossa > 20) // Else if -taikasana, joka kysyy: "Entä jos rahaa on enemmän kuin 20 euroa?"
{ // Myös else if vaatii samanlaiset sulkeet kuin if
	ostos = "Darude - Da Sandstorm" // Jos rahaa on tarpeeksi, osta levy
} // Toinen kaarisulje loppuun, jota myös else if kaipaa

else // Muussa tapauksessa, eli jos rahaa ei ole edes yli 20 euroa
{ // Else-taikasana vaatii myös kaarisulkeet
	ostos = "karkkia" // Ostetaan muussa tapauksessa karkkia koko rahalla!
} // Else-taikasanan päättävä kaarisulje
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			sano("Koska meillä on rahaa " + rahaaLompakossa + ", tulee ostettavaksi " + ostos + "!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}


BasicEquals.prototype = Object.create(MiniQuest.prototype);
BasicEquals.prototype.constructor = IfElse;


function BasicEquals(game) {
	var _this = this;

	this.init = function() {
		this.x = 12;
		this.y = 25;
		this.activeLevel = 4;
		
		this.questTitle = "Yhtäsuurien vertailu";
		this.shortDescription = "Varaa kapale Rihannaa tai Darudea varten";
		this.questDescription = hereDoc(function() {/*!
<div>
Kaupunkiin on tulossa esiintymään joko Rihanna tai Darude. Voisitko käyttää if ja else if -taikasanoja
siihen, että jos esiintyjä on Rihanna, teet uuden muuttujan <b>kappale</b> ja laitat sen arvoksi "Diamonds".
Muuten jos esiintyjä onkin Darude, niin kappale on Sandstorm
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var esiintyja, kappale;
*/});

		this.editorCode = hereDoc(function() {/*!
esiintyja = "Rihanna" // Rihanna on esiintyjänä. Tee if ja else if -taikasanat Rihannalle ja Darudelle!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			var originalSinger = esiintyja;
			var originalPiece = kappale;

			code = code.replace("esiintyja", "x");
			pieces = [];
			
			esiintyja = "Rihanna";
			kappale = undefined;
			eval(code);
			pieces.push(kappale);

			esiintyja = "Darude";
			kappale = undefined;
			eval(code);
			pieces.push(kappale);

			var pass = true;
			var rihanna = false;
			var darude = false;

			if (pieces.length != 2)
				pass = false;
			
			if (pieces.indexOf("Diamonds") == -1)
				pass = false;
			else
				rihanna = true
			
			if (pieces.indexOf("Sandstorm") == -1)
				pass = false;
			else
				darude = true;

			if (originalPiece == undefined)
				sano("Esiintyjä on " + originalSinger + ". Muuttujaa <b>kappale</b> ei kuitenkaan ole!");

			else
				sano("Esiintyjä on " + originalSinger + ". Saamme kuulla kappaleen " + originalPiece);

			if (rihanna == false)
				sano("Unohdit koodista Rihannan Diamonds-kappaleen.");

			if (darude == false)
				sano("Unohdit koodista Daruden Sandstorm-kappaleen.");

			if (pass == true) {
				sano("Koodisi varautuu sekä Rihannan tai Daruden tulemiseen, eli hyvä hyvä!")
				this.finishQuest();
			}
			else
				sano("Muistitko sekä if että else if -taikasanan koodiin?")

*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var lipunHinta, paikka;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Yhtäsuurien vertailu</h3>

<div>
Tähän mennessä if-taikasanoissa ollaan verrattu asioita niin, että onko jokin suurempi kuin jokin toinen.
Koodatessa voit tietysti verrata asioita niin, että onko jokin asia yhtä suuri.
<br /><br />
Verratessa kahta asiaa toisiinsa käytämme <b>kahta yhtäsuuruusmerkkiä, eli ==</b>. Ei siis yhtä, koska
sitähän käytettiin laittamaan muuttujille arvo!
</div>

<div class="codeDiv">
<textarea class="originalCode">
lipunHinta = "kallis" // Kuten muistat, muuttujille laitetaan arvo <b>yhdellä yhtäsuuruusmerkillä</b>

if (lipunHinta == 20) // Jos lipun hinta on yhtä kuin 20, niin paikka on katsomo D. Muista verratessa <b>kaksi yhtäsuuruusmerkkiä ==</b>
{ // Perus kaarisulkeet if:n kanssa
	paikka = "katsomo D" // Jos lipun hinta on 20, niin paikka on katsomo D
} // Perus kaarisulkeet if:n kanssa
else if (lipunHinta == "kallis") // Jos lipun hinta on "kallis". Voit siis aivan hyvin verrata lukujen lisäksi kirjaimia toisiinsa!
{ // Perus kaarisulkeet if:n kanssa
	paikka = "aitio B" // Paikka on aitio B, jos lipun hinta on "kallis"!
} // Perus kaarisulkeet if:n kanssa
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (lipunHinta != undefined && paikka == undefined)
				sano("Lipun hinta on " + lipunHinta + ". Paikkaa ei ole!");
			else if (lipunHinta == undefined || paikka == undefined)
				sano("Hmmm... muuttuja <b>lipunHinta</b> tai <b>paikka</b> puuttuu!");
			else
				sano("Lipun hinta on " + lipunHinta + ". Siispä otamme paikan " + paikka);
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BasicEquals.prototype = Object.create(MiniQuest.prototype);
BasicEquals.prototype.constructor = BasicEquals;


function BasicSmallerThan(game) {
	var _this = this;

	this.init = function() {
		this.x = 12;
		this.y = 30;
		this.activeLevel = 4;
		
		this.questTitle = "Pienempi kuin -vertailu";
		this.shortDescription = "";
		this.questDescription = hereDoc(function() {/*!
<div>
Tässä tehtävässä katsotaan kuinka lämmintä ulkona on. Tee muuttuja <b>takki</b> ja laita sille
arvo "toppatakki", jos lämpötila on alle 0 astetta. Jos lämpötila onkin alle 10 astetta, laita
muuttujan takki arvoksi "syystakki". Muussa tapauksessa takki-muuttujan arvo on "kesätakki".
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var lampotila, takki;
*/});

		this.editorCode = hereDoc(function() {/*!
lampotila = -3.5 // Miinusasteita ulkona. Voit tehdä negatiivisia lukuja laittamalla viivan eteen!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			var originalTemp = lampotila;
			var originalWear = takki;

			code = code.replace("lampotila", "x");
			pieces = [];
			
			lampotila = -1;
			takki = undefined;
			eval(code);
			pieces.push(takki);

			lampotila = 9;
			takki = undefined;
			eval(code);
			pieces.push(takki);

			lampotila = 11;
			takki = undefined;
			eval(code);
			pieces.push(takki);

			var pass = true;
			var winterWear = false;
			var autumnWear = false;
			var summerWear = false;

			if (pieces.length != 3)
				pass = false;
			console.log(pieces)
			if (pieces.indexOf("toppatakki") == -1)
				pass = false;
			else
				winterWear = true
			
			if (pieces.indexOf("syystakki") == -1)
				pass = false;
			else
				autumnWear = true;

			if (pieces.indexOf("kesätakki") == -1)
				pass = false;
			else
				summerWear = true;

			if (originalWear == undefined)
				sano("Lämpötila ulkona on " + originalTemp + ". Muuttujaa <b>takki</b> ei kuitenkaan ole!");

			else
				sano("Ulkona on " + originalTemp + " astetta. Laitetaan päälle " + originalWear);

			if (winterWear == false)
				sano("Toppatakki puuttuu koodista.");

			if (autumnWear == false)
				sano("Koodista puuttuu syystakki.");

			if (summerWear == false)
				sano("Kesätakkia ei ole koodissa.");

			if (pass == true) {
				sano("Koodisi näyttäisi toimivan kuten pitää!")
				this.finishQuest();
			}
			else
				sano("Muistitko sekä if, else if ja else -taikasanat koodiin?")
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var keskiarvo;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Pienempi kuin -vertailu</h3>

<div>
Koodatessa voidaan katsoa onko jokin asia toista pienempi. Sitä tehdään väkäsellä <, joka saattaa olla
tuttu matikan tunnilta. Sitä käytetään alla olevassa koodissa.
<br /><br />
Saatko lisättyä koodiin sellaisen vertailun muiden lisäksi, että jos keskiarvo on yli 9, niin opettaja
antaa jonkin erityisen kannustavan kommentin?
<br /><br />
Voit siis vapaasti sekoitella vertailutapoja keskenään, eli sinun ei ole pakko käyttää pelkästään
vaikkapa "pienempi kuin"-vertailua!
</div>

<div class="codeDiv">
<textarea class="originalCode">
keskiarvo = 7.6 // Kevättodistuksen keskiarvo

if (keskiarvo < 6) // Jos keskiarvo on pienempi kuin 6
{
	kommentti = "Menettelee..." // Opettajan kommentti keskiarvolle
}
else if (keskiarvo < 8) // Muuten jos keskiarvo on alle 8
{
	kommentti = "Hyvää työtä!" // Opettajan kommentti keskiarvolle
}
else // Jos keskiarvo ei ole alle 6 tai 8. Eli toisin sanoen suurempi kuin 8!
{
	kommentti = "Loistavaa työtä!" // Opettajan kommentti keskiarvolle
}
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (keskiarvo != undefined)
				sano("Kevättodistukseni keskiarvo oli " + keskiarvo);
			if (kommentti != undefined)
				sano('Sain opettajalta seuraavanlaisen kommentin: "' + kommentti + '"');
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BasicSmallerThan.prototype = Object.create(MiniQuest.prototype);
BasicSmallerThan.prototype.constructor = BasicSmallerThan;


function BasicGreaterThan(game) {
	var _this = this;

	this.init = function() {
		this.x = 7;
		this.y = 22;
		this.activeLevel = 4;
		
		this.questTitle = "Suurempi kuin -vertailu";
		this.shortDescription = "";
		this.questDescription = hereDoc(function() {/*!
<div>
Sinulla sattuu olemaan synttärit ja tarjoilet vieraillesi kahdenlaista herkkua: sipsejä ja banaanikakkua.
Pidät huolta, että yhden herkun vähentyessä haet lisää toista herkkua sen tilalle.
<br /><br />
Tee koodi, joka lisää sipsejä 150:llä, jos kakkkua on vähemmän kuin 0,5. Jos taas sipsejä on vähemmän
kuin 50, lisää kakkua 3:lla.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var sipseja, kakkua;
*/});

		this.editorCode = hereDoc(function() {/*!
sipseja = 100 // Sipsejä on aluksi 100
kakkua = 3.75 // Kakkua on alussa 3,75
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (sipseja == undefined)
				sano("Laita muuttuja <b>sipseja</b> koodiin.");

			else if (kakkua == undefined)
				sano("Laita muuttuja <b>kakkua</b> koodiin.");

			else {
				sano("Sipsejä on nyt " + sipseja + ". Kakkua on " + kakkua);

				newCode = code.replace("sipseja", "x").replace("kakkua", "y");

				var passed = true;

				kakkua = 0.4;
				sipseja = 100;
				eval(newCode);

				if (sipseja != 250) {
					sano("Muuttujaa <b>sipseja</b> täytyy lisätä 150:llä, jos kakkua on alle 0,5");
					passed = false;
				}

				kakkua = 1;
				sipseja = 49;
				eval(newCode);

				if (kakkua != 4 && passed != false) {
					sano("Muuttujaa <b>kakkua</b> täytyy lisätä 3:lla, josa sipsejä on alle 50");
					passed = false;
				}

				if (passed == true) {
					sano("Kakku-sipsi-koodisi toimii mahtavasti. Well done, sir!")
					this.finishQuest();
				}
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var lampotila, liikunta;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Suurempi kuin -vertailu</h3>

<div>
Asioita voidaan koodissa verrata myös niin, että katsotaan onko yksi asia suurempi kuin jokin toinen.
<br /><br />
Alla olevassa koodissa katsotaan onko ulkona alle -15 astetta lämmintä ja jos on, niin liikuntana on
sulista. Muuten mennään hiihtämään. Osaatko lisätä koodiin, että jos on alle -30 astetta, niin liikuntana
ei ole mitään, koska koulu on peruttu!
</div>

<div class="codeDiv">
<textarea class="originalCode">
lampotila = -5.2 // Ulkona on -5.2 astetta lämmintä

if (lampotila < -15) // Jos lämpötila on pienempi kuin -15 astetta
{ // If vaatii tällaiset kaarisulkeen alkuun
	liikunta = "sulkapalloa" // Liikuntatunnilla on sulkapalloa, jos on alle -15 astetta!
} // If haluaa myös, että kaarisulje laitetaan sen loppuun
else // Muussa tapauksessa
{
	liikunta = "hiihtoa" // Liikunta on hiihtoa
}
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (lampotila != undefined && liikunta != undefined)
				sano("Ulkona on " + lampotila + " astetta, joten liikuntana on " + liikunta);
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BasicGreaterThan.prototype = Object.create(MiniQuest.prototype);
BasicGreaterThan.prototype.constructor = BasicGreaterThan;


function BasicWhile(game) {
	var _this = this;

	this.init = function() {
		this.x = 2;
		this.y = 22;
		this.activeLevel = 5;
		
		this.questTitle = "While-taikasana";
		this.shortDescription = "";
		this.questDescription = hereDoc(function() {/*!
<div>
Löydät itsesi yllättäen marjametsästä. Otat askeleita pöpelikössä ja kullakin poimit 11 marjaa.
<br /><br />
Tee while-taikasana, jossa otat 100 askelta ja jokaisen askeleen kohdalla poimit 11 marjaa.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var askeleet, marjoja;
*/});

		this.editorCode = hereDoc(function() {/*!
askeleet = 0 // Askeleita on aluksi nolla
marjoja = 0 // Marjojakaan ei olla aluksi kerätty yhtään
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (askeleet == undefined)
				sano("Muuttujaa <b>askeleet</b> ei ole koodissa!");

			else if (marjoja == undefined)
				sano("Muuttujaa <b>marjoja</b> ei ole koodissa!");

			else if (askeleet == 100 && marjoja == 1100) {
				sano("Rämmimme metsässä 100 askelta ja keräsimme 1100 marjaa. Täältä tullaan marjapiirakka!");
				this.finishQuest();
			}

			else if (askeleet == 100 && marjoja != 1100)
				sano("Otimme kyllä 100 askelta, mutta marjoja on väärä määrä, eli " + marjoja);

			else if (askeleet != 100 && marjoja == 1100)
				sano("Saimme marjoja " + marjoja, " + mutta otimme väärän määrän askeleita, eli " + askeleet);

			else
				sano("Otimmet askeleita " + askeleet + " ja saimme marjoja " + marjoja + ". Tarkista, että askeleita on 100 ja marjoja kullakin askeleella 11.");
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var banaanejaSyoty, mansikoitaSyoty;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>While-taikasana</h3>

<div>
While-taikasana antaa meidän suorittaa samaa koodinpätkää monta kertaa, kertaa, kertaa, kertaa.
Jos kohtaat tilanteen, että sinulla olisi hillitön himo syödä yhdeksän banaania, voisit tehdä sen koodin
avulla kuten alla!
<br /><br />
Koita muuttaa while-taikasanan sisällä olevaa 9:ää joksikin muuksi. Koita lisäksi laittaa koodiin 
muuttuja nimeltään mansikoitaSyoty ja lisää siihen vaikkapa kaksi yhden sijaan!
</div>

<div class="codeDiv">
<textarea class="originalCode">
banaanejaSyoty = 0 // Banaaneja on aluksi syöty pyöreät nolla kappaletta

while (banaanejaSyoty < 9) // Toista kaarisulkeiden välissä olevaa niin kauan kuin banaaneja on syöty alle 9
{ // Kuten if-taikasanassa, myös while-taikasana kaipaa kaarisulkeita! Näiden sulkeiden välissä olevaa koodia toistetaan niin kauan kuin banaaneja on syöty alle 9
	banaanejaSyoty = banaanejaSyoty + 1 // Muuttujaan lisätään joka kierros yksi. Jos tätä ei olisi, while-taikasana ei koskaan loppuisi! Huomaatko minkä takia?
} // While-taikasanan loppuunkin on laitettava kaarisulje. Näiden sulkeiden välissä olevaa koodia toistetaan niin kauan kuin banaaneja on syöty alle 9
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (banaanejaSyoty != undefined)
				sano("Banaaneja on syöty kokonaiset " + banaanejaSyoty + " kappaletta!");

			if (isNaN(mansikoitaSyoty) == true)
				sano("Unohditko laittaa alkuun, että muuttuja <b>mansikoitaSyoty</b> on nolla?");
			
			else if (mansikoitaSyoty != undefined)
				sano("Söin samalla myös " + mansikoitaSyoty + " mansikkaa!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BasicWhile.prototype = Object.create(MiniQuest.prototype);
BasicWhile.prototype.constructor = BasicWhile;


function BaicLists(game) {
	var _this = this;

	this.init = function() {
		this.x = 9;
		this.y = 30;
		this.activeLevel = 6;
		
		this.questTitle = "Listat";
		this.shortDescription = "Tee lista kouluaineista";
		this.questDescription = hereDoc(function() {/*!
<div>
Tässä tehtävässä saat tehdä listan nimeltään <b>aineet</b>, johon listaat ainakin kolme lempiainettasi
koulusta. Kopioi sitten tuosta listasta muuttujaan <b>parasAine</b> se aine, josta pidät eniten!
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var aineet, parasAine;
*/});

		this.editorCode = hereDoc(function() {/*!
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (aineet == undefined)
				sano("Listaa <b>aineet</b> ei ole koodissa.");

			else if (typeof aineet != "object")
				sano("Muuttujan <b>aineet</b> täytyy olla lista!");
			else {
				sano("Lempiaineitani koulussa on " + aineet);

				if (parasAine == undefined)
					sano("Muuttujaa <b>parasAine</b> ei kuitenkaan vielä ole!");
				else if (aineet.indexOf(parasAine) == -1)
					sano("Parasta ainetta " + parasAine + " ei löydy ainelistasta!");
				else if (aineet.length > 2) {
					sano("Paras aine niistä on " + parasAine);
					this.finishQuest();
				}
				else
					sano("Laita listaan ainakin kolme koulun lempiainettasi.")
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var juomalista, lukuja, ruokaJuoma, limsa, mehu;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Listat</h3>

<div>
Listat ovat juuri miltä ne kuulostavatkin, eli listoja asioista, kuten ostoksista, vaatekaappisi
sisällöstä tai vaikka tyttö/poikakavereistasi!
<br ><br />
Listojen tekeminen on melko simppeli homma, kuten alla näkyy. Voimme myös kopioida asioita listasta
muuttujiin, kuten koodissa on tehty ruokaJuoma-muuttujalla. Huomaa, että lasku lähtee listoissa nollasta,
eli <b>"nollas" kohta listoissa on eka kohta</b>!
<br ><br />
Osaatko laittaa listaan jonkin uuden asian? Ota myös ylimääräinen 9,2 pois juomalistasta
(myös numeroita saa laittaa listoihin)! Yritä lopuksi kopioida "marjamehu" muuttujaan <b>mehu</b>.
</div>

<div class="codeDiv">
<textarea class="originalCode">
juomalista = ["maito", "marjamehu", "kokis", 9.2] // Tässä lista juomistamme, sir. Muista hakasulkeet [ ja ] listan päihin.

ruokaJuoma = juomalista[0] // Näin voidaan ottaa listasta "maito" muuttujaan ruokaJuoma. Listoissa lasku lähtee nollasta, joten "nollas" kohta onkin listojen eka kohta!
limsa = juomalista[2] // Otetaan vielä limsa toiseen muuttujaan
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (juomalista != undefined)
				sano("Juomalistalla näkyy olevan sellaiset juomat kuin " + juomalista);

			if (ruokaJuoma != undefined)
				sano("Ruomajuomaksi tuli " + ruokaJuoma);

			if (limsa != undefined)
				sano("Virvoitusjuomaksi valitsimme " + limsa);

			if (mehu != undefined)
				sano("Lopuksi mehuksi tulee " + mehu)
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

BaicLists.prototype = Object.create(MiniQuest.prototype);
BaicLists.prototype.constructor = BaicLists;


function ListPush(game) {
	var _this = this;

	this.init = function() {
		this.x = 7;
		this.y = 33;
		this.activeLevel = 6;
		
		this.questTitle = "Listoihin lisääminen (push)";
		this.shortDescription = "Lisää vaatekaappiisi vaatteita";
		this.questDescription = hereDoc(function() {/*!
<div>
Tee lista nimeltään <b>kouluvalineet</b>, johon laitat pari haluamaasi kouluvälinettä.
Tee myös if-taikasana, jolla katsot onko liikuntatunnilla luistelua. Jos näin on, laita
kouluvälineisiin vielä luistimet. Muussa tapauksessa laita kouluvälineisiin verkkarit.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var liikunta, kouluvalineet;
*/});

		this.editorCode = hereDoc(function() {/*!
liikunta = "luistelua" // Koulussa on luistelua
*/});

		this.finalizingCode = hereDoc(function() {/*!
			var originalSports = liikunta;
			var originalGear = kouluvalineet;

			if (originalGear == undefined)
				sano("Laita koodiin muuttuja <b>kouluvalineet</b>");
			else if (originalSports == undefined)
				sano("Laita koodiin muuttuja <b>liikunta</b>");
			else
				sano("Liikuntatunnilla on " + originalSports + ". Otan kouluun mukaan " + originalGear)

			code = code.replace("liikunta", "x");
			code = code.replace("kouluvalineet", "y");
			kouluvalineet = [];
			
			liikunta = "luistelua";
			eval(code);

			liikunta = "";
			eval(code);

			var pass = true;
			var iceskating = false;
			var otherSport = false;
			
			if (kouluvalineet.indexOf("luistimet") == -1)
				pass = false;
			else
				iceskating = true
			
			if (kouluvalineet.indexOf("verkkarit") == -1)
				pass = false;
			else
				otherSport = true;

			if (iceskating == false)
				sano("Unohdit koodista if-taikasanan luistelua varten.");

			if (otherSport == false)
				sano("Unohdit koodista else if -taikasanan muuta liikunta kuin luistelua varten.");

			if (pass == true) {
				sano("Koodi on oikein!");
				this.finishQuest();
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var matkalaukku, matkakohde;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Listoihin lisääminen</h3>

<div>
Kun teet aluksi listan, niin sitä ei ole pakko jättää sellaiseksi ikuisiksi ajoiksi, vaan voit lisätä
uusia asioita sinne. Tätä varten on push-taikasana.
</div>

<div class="codeDiv">
<textarea class="originalCode">
matkalaukku = ["passi", "hammasharja", "t-paita"] // Matkalaukun sisältö

matkakohde = "Tokio" // Matkakohteena on Tokio

if (matkakohde == "Lontoo") // Jos matkakohde on Lontoo...
{
	matkalaukku.push("sateenvarjo") // Pakataan matkalaukkuun sateenvarjo käyttäen push-taikasanaa
}
else if (matkakohde == "Tokio") // Muuten jos matkakohde onkin Tokio
{
	matkalaukku.push("sanakirja") // Pakkaa tokioon mukaan sanakirja push-taikasanalla
}
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (matkalaukku != undefined) {
				if (matkakohde == undefined)
					sano("Meillä ei ole vain matkakohdetta koodissa!");
				else
					sano("Matkakohde on " + matkakohde);

				sano("Matkalaukussa on seuraavat tavarat: " + matkalaukku);
			}
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

ListPush.prototype = Object.create(MiniQuest.prototype);
ListPush.prototype.constructor = ListPush;


function ListSplice(game) {
	var _this = this;

	this.init = function() {
		this.x = 6;
		this.y = 41
		this.activeLevel = 6;
		
		this.questTitle = "Listoista poistaminen (splice)";
		this.shortDescription = "";
		this.questDescription = hereDoc(function() {/*!
<div>
Olet rantakioskin pitäjä ja sinulla on listallasi juomia asiakkaittesi iloksi. Tiedät kuitenkin, että
eräinä päivinä asiakkaina on vain limsoja inhoavat, ja muina päivinä taas mehuja inhoavat asikkaat.
<br ><br />
Tee koodi, joka poistaa juomalistalta jaffan ja kokiksen, jos muuttuja <b>poistettava</b> on
"limsa". Poista mansikkamehu, jos muuttujan arvo onkin "mehu". Käytä splice-taikasanaa.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var juomia, poistettava;
*/});

		this.editorCode = hereDoc(function() {/*!
juomia = ["jaffa", "kokis", "mansikkamehu"] // Lista juomista
poistettava = "limsa" // Poistettava juomatyyppi
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (juomia == undefined)
				sano("Listaa <b>juomia</b> ei ole koodissa");

			else if (poistettava == undefined)
				sano("Muuttujaa <b>poistettava</b> ei ole koodissa");

			else {
				sano("Listassa on nämä juomat: " + juomia);

				var otherCode = code;
				otherCode = otherCode.replace("poistettava", "x");
				
				poistettava = "limsa";
				eval(otherCode);

				var soda = true;
				if (juomia.length != 1 || juomia[0] != "mansikkamehu")
					soda = false;

				poistettava = "mehu";
				eval(otherCode);

				var juice = true;
				if (juomia.length != 2 || juomia.indexOf("jaffa") == -1 || juomia.indexOf("kokis") == -1)
					juice = false

				if (soda == true && juice == true) {
					sano("Koodi näyttäisi olevan aivan oikein!");
					this.finishQuest();
				}

				if (soda == false)
					sano("Teitkö limsojen poistamisen oikein if-taikasanaa käyttäen?");

				if (juice == false)
					sano("Teitkö mehujen poistamisen oikein if-taikasanan avulla?");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var karkit;
*/});

		this.theoryPage = $(hereDoc(function() {/*
<div>

<h3>Listoista poistaminen</h3>

<div>
Tähän mennessä listoihin ollaan lisätty asioita push-taikasanalla. Listoista voidaan aivan hyvin myös
poistaa juttuja, ja sehän käy päinsä <b>splice</b>-taikasanalla.
<br /><br />
Alla olevassa koodissa on lista karkeista. Koska kukaan ei voi pitää rusinasydämestä, otetaan se pois
listalta splice:n avulla.
<br /><br />
Yritä muuttaa splice-taikasanan sulkeissa olevaa toinen luku kakkoseksi. Mitä silloin tapahtuu? Koita
lisäksi poistaa muutkin karkit (vieläpä vain yhdellä splice-taikasanalla, jos keksit miten)!
</div>

<div class="codeDiv">
<textarea class="originalCode">
var karkit = ["merkkari", "toffeekuutio", "rusinasydän", "nallekarkki"] // Lista karkeista, jossa on ällö rusinasydän... hyh.

karkit.splice(2, 1) // Poista kolmas kohta listasta (eli kohta 2, kun lasketaan nollasta lähtien)
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (karkit != undefined)
				sano("Karkkeja on nyt: " + karkit);
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

ListWhile.prototype = Object.create(MiniQuest.prototype);
ListWhile.prototype.constructor = ListSplice;


function ListWhile(game) {
	var _this = this;

	this.init = function() {
		this.x = 12;
		this.y = 37;
		this.activeLevel = 6;
		
		this.questTitle = "Listojen läpikäyminen while-taikasanalla";
		this.shortDescription = "";
		this.questDescription = hereDoc(function() {/*!
<div>
Olet tehnyt kovasti hommia koulussa, joten olet ansainnut arvosanojesi nousun. Tee while-taikasana,
joka nostaa <b>arvosanat</b>-listassa olevia arvosanoja kahdella ja laittaa uudet arvosanat toiseen
listaan, jonka nimi on <b>uudetArvosanat</b>.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var arvosanat, uudetArvosanat;
*/});

		this.editorCode = hereDoc(function() {/*!
arvosanat = [7, 5, 7, 6, 8] // Lista joistakin kouluarvosanoista
uudetArvosanat = [] // Tyhjä arvosanalista
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (arvosanat == undefined)
				sano("Listaa <b>arvosanat</b> ei ole koodissa!");

			else if (uudetArvosanat == undefined)
				sano("Listaa <b>uudetArvosanat</b> ei ole koodissa!");

			else {
				if (uudetArvosanat.length == 0)
					sano("Vanhat arvosanat olivat " + arvosanat +". Korotetuissa arvosanoissa ei ole mitään!");
				else
					sano("Vanhat arvosanat olivat " + arvosanat +". Korotetut taas ovat " + uudetArvosanat);
				var newCode = code.replace("arvosanat", "x");

				// Create a new array with same size as the original arvosanat
				newRatings = [];
				raisedRatings = [];

				for (var i in arvosanat) {
					newRatings.push(0);
					raisedRatings.push(2);
				}

				arvosanat = newRatings;
				eval(newCode);

				// Are the new arrays the same?
				if (uudetArvosanat.toString() == raisedRatings.toString()) {
					sano("Koodisi toimii moitteettomasti ja arvosanat sen kun kasvavat!");
					this.finishQuest();
				}
				else
					sano("Koodi ei toimi aivan kuten pitäisi. Teitkö while-taikasanan oikein? Voit ottaa mallia edelliseltä sivulta.");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var pizzaLista, paistetut, pizzaLaskuri, palaneet;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Listojen läpikäyminen while-taikasanalla</h3>

<div>
Jos sinulla sattuu olemaan hullun pitkä lista, kuten pizzeriassa pizzoja, voit haluta käydä niitä
jotenkin helpommin läpi kuin yksitellen. Tätä varten voidaan käyttää while-taikasanaa.
<br /><br />
Alla olevassa koodissa käydään pizzalista läpi while:n avulla ja laitetaan kukin pizza paistettujen
pizzojen listaan. Tässä ei tule mitään uutta taikasanaa, vaan ennemmin näytetään miten monia aiemmin
näytettyjä juttuja voidaan yhdistää!
<br /><br />
Tunnistatko ja ymmärrätkö kullakin rivillä olevat asiat? Osaatko laittaa listan nimeltään <b>palaneet</b>
koodiin ja laittaa jonkin pizzoista sinne, eikä paistettuihin? (Vaikkapa Hawaii)
</div>

<div class="codeDiv">
<textarea class="originalCode">
pizzaLista = ["Tuplajuusto", "Vegetariana", "Mamma Mia", "Hawaii"] // Lista ihanaisista pizzoista

paistetut = [] // Tyhjä lista, johon laitamme paistetut pizzat

pizzaLaskuri = 0 // Pidä kirjaa siitä, monesko pizza on menossa

while (pizzaLaskuri < 4) // Käydään pizzoja läpi niin monta kuin niitä on listalla
{ // While kaipaa tällaista suljetta alkuun
	pizzanNimi = pizzaLista[pizzaLaskuri] // Katso mikä pizza on pizzalaskurin kohdalla pizzalistalla

	paistetut.push(pizzanNimi) // Laita paistettu pizza omaan listaansa

	pizzaLaskuri = pizzaLaskuri + 1 // Lisää laskuriin yksi, tai paistamme samaa pizzaa ikuisesti!
} // While:n loppuunkin on laitettava kaarisulje
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (paistetut != undefined)
				sano("Paistoimme pizzat ja nyt meillä on valmiina nämä pizzat: " + paistetut);

			if (palaneet != undefined)
				sano("Oh noes, meiltä paloi nämä pizzat: " + palaneet + "!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

ListWhile.prototype = Object.create(MiniQuest.prototype);
ListWhile.prototype.constructor = ListWhile;


function FunctionBasic(game) {
	var _this = this;

	this.init = function() {
		this.x = 2;
		this.y = 20;
		this.activeLevel = 7;
		
		this.questTitle = "Function-taikasana";
		this.shortDescription = "Tee funktio banaanin syönnille";
		this.questDescription = hereDoc(function() {/*!
<div>
Nyt kun funktio-taikasana on esitelty, voit kirjoittaa saman tien omasi! Koita tehdä funktio nimeltään
<b>mussutaBanaani</b>, jota kutsumalla lisäämme muttujaan <b>syodytBanaanit</b> yksi joka kerta kun
kutsumme funktiota.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var syodytBanaanit, mussutaBanaani;
*/});

		this.editorCode = hereDoc(function() {/*!
syodytBanaanit = 0 // Alussa banaaneja ei ole syöty yhtään
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (syodytBanaanit == undefined)
				sano("Muuttuja <b>syodytBanaanit</b> ei ole koodissa.");

			else if (mussutaBanaani == undefined)
				sano("Funktio-taikasanaa <b>mussutaBanaani</b> ei ole koodissa. Koita tehdä sen niminen funktio!");

			else if (syodytBanaanit == 0)
				sano("Kokeilitko kutsua tekemääsi funktiota? Koita vielä sitä!");

			else {
				sano("Banaaneja on syöty " + syodytBanaanit + " kappaletta!");

				newCode = code.replace("syodytBanaanit", "x");
				syodytBanaanit = 0;

				var pass = true;

				try {
					mussutaBanaani();
				}
				catch (e) {
					pass = false;
				}

				if (syodytBanaanit <= 0)
					pass = false;

				if (pass == true) {
					sano("Banaaneja mussuttava funktio-taikasana toimii loistavasti!");
					this.finishQuest();
				}
				else
					sano("Funktio <b>mussutaBanaani</b> ei toimi aivan kuin pitäisi. Lisäätkö syötyihin banaaneihin mitään?");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var pomppuja, kyykkyja, pompitaan;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Function-taikasana</h3>

<div>
Function-taikasana (tai hieman helpommin funktio-taikasana) on hieman hankalan niminen, mutta kätevä juttu.
Kuvittele, että haluaisimme pompata, eli siis hypätä, milloin huvittaa. Miten tekisimme sen?
<br /><br />
Tätä varten on funktio-taikasana, jonka avulla voimme kirjoittaa koodin pomppimista varten kerran ja
käyttää sitä koodia sitten milloin haluamme!
<br /><br />
Alla olevassa koodissa teemme pomppimista aiheuttavan funktio-taikasanan ja alempana kutsumme sitä (eli suoritamme sen koodin).
Osaatko kutsua funktiota monesti, että pomppuja tulee neljä? Entä osaatko lisätä muuttujan <b>kyykkyja</b> funktioon ja
lisätä siihen jonkin luvun kuin pompuissa?
</div>

<div class="codeDiv">
<textarea class="originalCode">
pomppuja = 0 // Alussa pomppuja ei vielä ole yhtään!

function pompitaan() // Tehdään <b>pompitaan</b>-niminen funktio-taikasana, joka saa kutsujansa pomppaamaan!
{ // Funktio-taikasanakin vaatii kaarisulkeen eteensä
	pomppuja = pomppuja + 1 // Lisää pomppuihin yksi kerta kun nyt halusimme pomppia!
} // Funktio-taikasanan loppuunkin täytyy laittaa kaarisulje

pompitaan() // Tässä käsketään suorittamaan pompitaan-funktion koodi. Tämän tempun nimi on <b>funktion kutsuminen</b>

</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (pomppuja != undefined) {
				if (pomppuja == 1)
					sano("Olemme pomppineet 1 kerran");

				else
					sano("Olemme pomppineet " + pomppuja + " kertaa");
			}
			if (kyykkyja != undefined) {
				if (isNaN(kyykkyja) == true)
					sano("Muuttujassa <b>kyykkyja</b> on jotakin vialla. Yritä korjata se.");

				else if (kyykkyja == 1)
					sano("Olemme myös kyykänneet 1 kerran");

				else
					sano("Olemme myös kyykänneet " + kyykkyja + " kertaa");
			}
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

FunctionBasic.prototype = Object.create(MiniQuest.prototype);
FunctionBasic.prototype.constructor = ListSplice;


function FunctionParameters(game) {
	var _this = this;

	this.init = function() {
		this.x = 9;
		this.y = 17;
		this.activeLevel = 7;
		
		this.questTitle = "Funktioiden parametrit";
		this.shortDescription = "Piirakoita hotkiva funktio";
		this.questDescription = hereDoc(function() {/*!
<div>
Aiemmin teit banaaninmussutusta harrastavan funktion. Tee tähän tehtävään piirakoita syövä funktio
nimeltään <b>hotkiPiirakka</b>.
<br /><br />
Funktio ottaa parametriksi piirakoiden määrän ja lisää sen <b>syodytPiirakat</b>-muuttujaan.
<br /><br />
Funktio ei kuitenkaan pidä jos sille annetaan liian vähän piirakoita. Tee siis funktion sisään if-taikasana,
joka katsoo onko parametri suurempi kuin 3 ja lisää tällöin parametrin arvon syodytPiirakat-muuttujaan.
Sinun ei tarvitse käyttää else if tai else -taikasanoja, vaan pelkästään if-taikasanaa.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var syodytPiirakat, hotkiPiirakka;
*/});

		this.editorCode = hereDoc(function() {/*!
syodytPiirakat = 0 // Alussa emme ole syöneet yhtään piirakkaa
*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (syodytPiirakat == undefined)
				sano("Muuttujaa <b>syodytPiirakat</b> ei ole koodissa.");

			else if (hotkiPiirakka == undefined)
				sano("Funktiota <b>hotkiPiirakka</b> ei ole koodissa.");

			else {
				if (syodytPiirakat == 0)
					sano("Olen syönyt pyöreät 0 piirakkaa!");
				else
					sano("Omnomon, olen pistänyt naamaani " + syodytPiirakat + " piirakkaa!");

				var passed = true;

				eval(code);


				syodytPiirakat = 0;
				hotkiPiirakka(4);

				if (syodytPiirakat != 4) {
					sano("Funktio ei näytä lisäävän parametrin arvoa <b>syodytPiirakat</b>-muuttujaan.");
					passed = false;
				}

				syodytPiirakat = 0;
				hotkiPiirakka(2);

				if (syodytPiirakat != 0 && passed != false) {
					sano("Lisää parametria syodytPiirakat-muuttujaan vain, jos sen arvo on suurempi kuin 3. Käytä if-taikasanaa tämän tarkistamiseen.");
					passed = false;
				}

				if (passed == true) {
					sano("Piirakanhotkijasi toimii loisteliaasti!");
					this.finishQuest();
				}
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var pomppuja, haarapomppuja;
*/});

		this.theoryPage = $(hereDoc(function() {/*!
<div>

<h3>Funktioiden parametrit</h3>

<div>
Okei, mitä jos haluatkin välillä tehdä tavallisten pomppujen lisäksi haarapomppuja?
Tämä onnistuu <b>parametreilla</b>, jotka kertovat funktiolle tarkemmin mitä sen pitäisi tehdä.
<br /><br />
Alla olevassa pompitaan-funktiossa on tehty juuri näin. Parametri nimeltään <b>pomppuTyyli</b>
ottaa tiedon siitä, minkälaisen pompun haluamme tehdä. pomppuTyyli toimii funktion sisällä aivan kuin
normaali muuttuja, se vain saa arvon pompitaan-funktiota kutsuttaessa!
<br /><br />
Kokeile aluksi koodia. Yritä sitten kutsua funktiota parametrilla "megapomppu". Mitä tapahtuu?
</div>

<div class="codeDiv">
<textarea class="originalCode">
pomppuja = 0 // Pomppuja on alussa nolla
haarapomppuja = 0 // Haarapomppuja on alussa 0

function pompitaan(pomppuTyyli) // Tehdään <b>pompitaan</b>-niminen funktio, jonka parametrina on <b>pomppuTyyli</b>. pomppuTyyli toimii funktion sisällä aivan kuin mikä tahansa muuttuja!
{ // Funktio-taikasanan vaatima kaarisulje alkuun
	if (pomppuTyyli == "haarapomppu") // Voimme aivan hyvin laittaa vaikkapa if-taikasanan funktion sisäll! Tässä katsotaan onko <b>pomppuTyyli</b>-parametrin arvo "haarapomppu"
	{
		haarapomppuja = haarapomppuja + 1 // Lisätään haarapomppuihin yksi!
	}

	else // Muussa tapauksessa jos ei käsketty tehdä haarapomppuja
	{
		pomppuja = pomppuja + 1 // Tehdään tavallinen pomppu
	}
} // Myös funktion lopussa täytyy käyttää kaarisuljetta

pompitaan("haarapomppu") // Kutsutaan <b>pompitaan</b>-funktiota ja käsketään sen tehdä haarapomppuja! "haarapomppu" tulee siis parametrin <b>pomppuTyyli</b> arvoksi!

pompitaan("pomppu") // Tässä taas <b>pompitaan</b>-funktio laitetaan tekemään taas normaalia pomppua. Parametri <b>pomppuTyyli</b> saa näin arvon "pomppu".
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (pomppuja != undefined)
				sano("Teimme " + pomppuja + " tavallista pomppua");

			if (haarapomppuja != undefined)
				sano("Haarapomppuja tuli tehtyä " + haarapomppuja + "!");
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

FunctionParameters.prototype = Object.create(MiniQuest.prototype);
FunctionParameters.prototype.constructor = FunctionParameters;


function FunctionReturn(game) {
	var _this = this;

	this.init = function() {
		this.x = 7;
		this.y = 34;
		this.activeLevel = 7;
		
		this.questTitle = "Funktioiden palauttama arvo";
		this.shortDescription = "Funktio, joka korottaa arvosanaasi";
		this.questDescription = hereDoc(function() {/*!
<div>
Tee <b>korota</b>-niminen funktio-taikasana, joka korottaa kouluarvosanaasi. Funktiolle
annetaan arvosana parametrina, johon se lisää kaksi. Lopulta funktio palauttaa korotetun arvosanan.
Aseta korotettu arvosana muuttujan <b>uusiArvosana</b> arvoksi.
</div>
*/});
		this.stagingCode = hereDoc(function() {/*!
			var uusiArvosana, korota;
*/});

		this.editorCode = hereDoc(function() {/*!

*/});

		this.finalizingCode = hereDoc(function() {/*!
			if (korota == undefined)
				sano("Funktiota nimeltään <b>korota</b> ei ole koodissa.");
			else {
				if (uusiArvosana != undefined)
					sano("Uusi arvosanasi on täten " + uusiArvosana)

				var rate = korota(6)

				if (rate == 6) {
					sano("Korottimesi toimii sulavasti!")
					this.finishQuest();
				}
				else if (rate == undefined)
					sano("Funktio ei näytä palauttavan korotettua arvosanaa.");
				else
					sano("Funktio ei korota arvosanaa kahdella.");
			}
*/});

		this.theoryStagingCode = hereDoc(function() {/*!
			var pomppu, ekaPomppu, tokaPomppua;
*/});

		this.theoryPage = $(hereDoc(function() {/*!

<div>

<h3>Funktioiden palauttama arvo</h3>

<div>
Funktioista on vielä yksi varteenotettava juttu sanomatta, eli niiden palautusarvo. Mietitäänpä taas
pomppufunktiota. Mitä jos halajatkin antaa funktiolle tekemiesi pomppujen määrän ja annat funktion päättää
mitä pomppuja teet?
<br /><br />
Voit tehdä sen <b>return</b>-taikasanalla, joka palauttaa funktiosta jonkin halutun jutun ulos. Alla
olevassa koodissa pompitaan-funktio palauttaa lopuksi pomppuTyyppi-muuttujan arvon, jonka funktion
kutsuja saa omakseen.
<br /><br />
Osaatko muuttaa funktiota niin, että se ei palautakaan pompputyyppiä, vaan lisää pomppujen määrään
jotakin ja palauttaa uusien pomppujen määrän?
</div>

<div class="codeDiv">
<textarea class="originalCode">

function pompitaan(montakoPomppua) // Funktio <b>pompitaan</b> ottaa tällä kertaa parametrin nimeltään <b>montakoPomppua</b>
{ // Funktion vaatima kaarisulje alkuun
	if (montakoPomppua > 5) // Onko pomppujen määrä enemmän kuin 5?
	{
		pomppuTyyppi = "normipomppu" // Tehdään normipomppuja, jos pomppuja tehdään enemmän kuin 5
	}
	else // Muussa tapauksessa...
	{
		pomppuTyyppi = "haarapomppu" // Tehdään haarapomppuja!
	}

	return pomppuTyyppi // Palautetaan taikasanalla <b>return</b> muuttujan <b>pomppuTyyppi</b> arvo kutsujalle!
} // Myös funktion lopussa täytyy käyttää kaarisuljetta

ekaPomppu = pompitaan(10) // Kutsutaan <b>pompitaan</b>-funktiota ja pyydetään pomppia 10 kertaa. Muuttujan <b>ekaPomppu</b> arvoksi tulee "normipomppu"!

tokaPomppu = pompitaan(3) // Pyydetään <b>pompitaan</b>-funktiota tekemään 3 pomppua. <b>tokaPomppu</b>-muuttuja saa arvon "haarapomppu"!
</textarea>
</div>

</div>

*/}));

		this.theoryFinalizingCode = hereDoc(function() {/*!
			if (ekaPomppu != undefined) {
				var t = typeof ekaPomppu;

				if (t == "string")
					sano("Eka pomppu on " + ekaPomppu);

				else if (t == "number")
					sano("Ekoja pomppuja teemme " + ekaPomppu + " kipaletta!");
			}

			if (tokaPomppu != undefined) {
				var t = typeof tokaPomppu;

				if (t == "string")
					sano("Toka pomppu on taasen " + tokaPomppu);

				else if (t == "number")
					sano("Toisia pomppuja tulee " + tokaPomppu + ". Muahahahaa!");
			}
*/});

		MiniQuest.call(this, game);
	}

	this.init()
}

FunctionReturn.prototype = Object.create(MiniQuest.prototype);
FunctionReturn.prototype.constructor = FunctionReturn;