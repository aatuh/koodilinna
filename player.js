
function Character(game, tileset, x, y, color, name) {
	this.init = function() {
		this.game = game;
		this.name = name;

		if (color == undefined)
			this.color = "white";
		else
			this.color = color;

		if (name == undefined)
			this.name = "Tyyppi";
		else
			this.name = name

		// Neutral standing positions in the character image
		this.directions = {
			up: 8,
			left: 9,
			down: 10,
			right: 11,
		};

		this.walkFrames = 7;
		this.moveSpeed = 3;
		this.characterX = x * 32 + 16;
		this.characterY = y * 32;

		this.moveDirection = "";
		this.moveDirectionX = 0;
		this.moveDirectionY = 0;
		
		this.sprite = this.game.animationsController.newAnimation({
			src: tileset,
			width: 832,
			height: 64,
			frames: 7,
			frameWidth: 64,
			frameHeight: 64,
			animationLine: 9,
			x: this.characterX,
			y: this.characterY
		});
	}

	this.getPosition = function() {
		return {x: (this.characterX - 16) / 32, y: this.characterY / 32};
	}

	// Actually changes character sprite position
	this.changePosition = function(x, y) {
		this.characterX = x * 32;
		this.characterY = y * 32;

		this.sprite.x = this.characterX;
		this.sprite.y = this.characterY;
	}

	this.changeFacing = function(faceString) {
		face = this.directions[faceString];
		this.sprite.setAnimationLine(face);

		this.moveDirection = faceString;
	}

	this.getFacing = function() {
		if (this.moveDirection == "")
			return "left";

		return this.moveDirection;
	}

	this.init();
}

function Npc(game, tileset, x, y, sayFirst, saySecond, sayCallback, color, name, quest) {

	var _this = this;

	this.init = function() {
		Character.call(this, game, tileset, x, y, color, name)
		
		this.quest = quest;

		this.talkInterval = 1000;
		this.lastTalk = Date.now();

		this.sayFirst = sayFirst;
		this.saySecond = saySecond;

		this.firstTold = false;

		this.sayCallback = sayCallback;
		this.sayCallbackDone = false;

		npcBlock = this.game.getNpcBlock();

		var pos = this.getPosition();
		var posX = pos.x;
		var posY = pos.y;

		var opts = {
			src: "tileset.png",
			x: posX + 1,
			y: posY
		}

		// TODO: Put this in a callback instead, not time based
		var timeOut = setTimeout(function() {
			_this.npcBlock1 = new npcBlock(_this, opts);
			_this.game.level.levelObjects.push(_this.npcBlock1);

			opts.y = opts.y + 1;
			_this.npcBlock2 = new npcBlock(_this, opts);
			_this.game.level.levelObjects.push(_this.npcBlock2);
		}, 1000);
	}

	this.talk = function() {
		var now = Date.now();
		if (now - this.lastTalk < this.talkInterval)
			return;

		this.lastTalk = Date.now();

		var pos = this.getPosition();
		var x = pos.x;
		var y = pos.y;

		if (this.firstTold == false) {
			this.game.createSpeech(this, this.sayFirst);
			this.firstTold = true;
		}
		else
			this.game.createSpeech(this, this.saySecond);

		if (this.sayCallback != undefined && this.sayCallbackDone == false) {
			this.sayCallbackDone = true
			this.sayCallback(this);
		}

		if (this.quest != undefined)
			this.game.displayQuest(this.quest);
	}

	this.playerWalk = function() {
		this.talk();

		return false;
	}

	this.init();
}

Npc.prototype = Object.create(Character.prototype);
Npc.prototype.constructor = Npc;

function Player(game, tileset, characterSex) {
	var _this = this;

	this.init = function() {
		Character.call(this, game, tileset)

		if (characterSex == "male") {
			this.color = "#6ea1F7";
			this.name = "Velho";
		}
		else {
			this.color = "#d27e82";
			this.name = "Velhotar"
		}

		this.keys = [];
		
		this.map = null;
		this.levelObjects = [];

		// Feet dimensions and position for wall collision detection
		this.spriteFeetOffsetUp = 54;
		this.spriteFeetOffsetLeft = 21;
		this.spriteFeetOffsetRight = this.spriteFeetOffsetLeft + 22;
		this.spriteFeetOffsetDown = this.spriteFeetOffsetUp + 9;

		this.allowMove = true;

		this.keyUpCallback, this.keyDownCallback;
	}

	this.stopMoving = function() {
		this.clearCallbacks();
		this.keys = [];
		this.sprite.stopAnimation();
	}

	/*
	this.deBindKeys = function() {
		window.removeEventListener("keydown", function() {});
		window.removeEventListener("keyup", function() {});
		this.clearCallbacks();
		this.keys = [];
		this.sprite.stopAnimation();
	}
	*/

	this.setAllowMove = function(allow) {
		this.allowMove = allow;
	}

	this.keyPressed = function(key) {

		// Up, left, down, right
		if (key == 38) {
			if (_this.moveDirection == "char_up")
				return;

			_this.keys.push("char_up");
			_this.moveUp();

			if (_this.keyDownCallback != undefined)
				_this.keyDownCallback();
		}
		else if (key == 37) {
			if (_this.moveDirection == "char_left")
				return;

			_this.keys.push("char_left");
			_this.moveLeft();

			if (_this.keyDownCallback != undefined)
				_this.keyDownCallback();
		}
		else if (key == 40) {
			if (_this.moveDirection == "char_down")
				return;

			_this.keys.push("char_down");
			_this.moveDown();

			if (_this.keyDownCallback != undefined)
				_this.keyDownCallback();
		}
		else if (key == 39) {
			if (_this.moveDirection == "char_right")
				return;

			_this.keys.push("char_right");
			_this.moveRight();

			if (_this.keyDownCallback != undefined)
				_this.keyDownCallback();
		}
	}

	this.keyReleased = function(key) {

		// Up, left, down, right
		if (key == 38) {
			_this.game.clearCallback("char_up");
			_this.removeKey("char_up");
		}
		else if (key == 37) {
			_this.game.clearCallback("char_left");
			_this.removeKey("char_left");
		}
		else if (key == 40) {
			_this.game.clearCallback("char_down");
			_this.removeKey("char_down");
		}
		else if (key == 39) {
			_this.game.clearCallback("char_right");
			_this.removeKey("char_right");
		}

		// No keys pressed
		if (_this.keys.length == 0) {
			_this.moveDirection = "";
			_this.sprite.stopAnimation();

			if (_this.keyUpCallback != undefined)
				_this.keyUpCallback();
		}
		// Else keep moving in the last pressed direction
		else {
			key = _this.keys[_this.keys.length - 1];
			if (key == "char_up")
				_this.moveUp();
			else if (key == "char_left")
				_this.moveLeft();
			else if (key == "char_down")
				_this.moveDown();
			else if (key == "char_right")
				_this.moveRight();
		}
	}

	this.setLevel = function(level) {
		this.level = level;
		this.map = level.map;
		this.levelObjects = level.getLevelObjects();

		this.changeFacing(level.playerFacing);
		this.changePosition(level.playerStartX, level.playerStartY);
	}

	this.moveUp = function() {
		_this.moveDirection = "char_up";

		_this.sprite.animationLine = _this.directions.up;
		_this.sprite.startAnimation();

		_this.moveDirectionX = 0;
		_this.moveDirectionY = -_this.moveSpeed;

		_this.clearCallbacks();
		_this.game.setCallback(_this.keepMoving, "char_up");
	}

	this.moveLeft = function() {
		_this.moveDirection = "char_left";

		_this.sprite.animationLine = _this.directions.left;
		_this.sprite.startAnimation();

		_this.moveDirectionX = -_this.moveSpeed;
		_this.moveDirectionY = 0;
		
		_this.clearCallbacks();
		_this.game.setCallback(_this.keepMoving, "char_left");
	}

	this.moveDown = function() {
		_this.moveDirection = "char_down";

		_this.sprite.animationLine = _this.directions.down;
		_this.sprite.startAnimation();

		_this.moveDirectionX = 0;
		_this.moveDirectionY = _this.moveSpeed;
		
		_this.clearCallbacks();
		_this.game.setCallback(_this.keepMoving, "char_down");
	}

	this.moveRight = function() {
		_this.moveDirection = "char_right";

		_this.sprite.animationLine = _this.directions.right;
		_this.sprite.startAnimation();

		_this.moveDirectionX = _this.moveSpeed;
		_this.moveDirectionY = 0;
		
		_this.clearCallbacks();
		_this.game.setCallback(_this.keepMoving, "char_right");
	}

	this.removeKey = function(key) {
		for (i = 0; i < this.keys.length; i += 1) {
			compare = this.keys[i];
			if (compare == key) {
				this.keys.splice(i, 1);
			}
		}
	}

	this.clearCallbacks = function() {
		_this.game.clearCallback("char_up");
		_this.game.clearCallback("char_right");
		_this.game.clearCallback("char_down");
		_this.game.clearCallback("char_left");
	}

	this.keepMoving = function() {
		_this.changeCharacterPosition();
	}

	this.changeCharacterPosition = function(x, y) {
		var newPositionX = this.characterX + this.moveDirectionX,
			newPositionY = this.characterY + this.moveDirectionY;

		var passable = true;

		if (this.moveDirection == "char_down") {

			var tileLeftOffset = Math.floor((newPositionX + this.spriteFeetOffsetLeft) / 32)
			var tileRightOffset = Math.floor((newPositionX + this.spriteFeetOffsetRight) / 32)
			var tileDownOffset = Math.floor((newPositionY + this.spriteFeetOffsetDown)  / 32);

			var tileLeft = this.map.map[tileLeftOffset][tileDownOffset];
			var tileRight = this.map.map[tileRightOffset][tileDownOffset];

			// Check level object collision
			for (i in this.levelObjects) {
				var levelObject = this.levelObjects[i];
				if ((levelObject.x == tileLeftOffset && levelObject.y == tileDownOffset) ||
					(levelObject.x == tileRightOffset && levelObject.y == tileDownOffset))
					passable = levelObject.playerWalk(this);
			}

			if (tileLeft == null || tileRight == null)
				passable = false;
			else if (tileLeft.passable == false || tileRight.passable == false)
				passable = false;
		}

		else if (this.moveDirection == "char_up") {
			var tileLeftOffset = Math.floor((newPositionX + this.spriteFeetOffsetLeft) / 32)
			var tileRightOffset = Math.floor((newPositionX + this.spriteFeetOffsetRight) / 32)
			var tileUpOffset = Math.floor((newPositionY + this.spriteFeetOffsetUp)  / 32);

			var tileLeft = this.map.map[tileLeftOffset][tileUpOffset];
			var tileRight = this.map.map[tileRightOffset][tileUpOffset];

			// Check level object collision
			for (i in this.levelObjects) {
				var levelObject = this.levelObjects[i];
				if ((levelObject.x == tileLeftOffset && levelObject.y == tileUpOffset) ||
					(levelObject.x == tileRightOffset && levelObject.y == tileUpOffset))
					passable = levelObject.playerWalk(this);
			}

			if (tileLeft == null || tileRight == null)
				passable = false;
			else if (tileLeft.passable == false || tileRight.passable == false)
				passable = false;
		}

		else if (this.moveDirection == "char_right") {
			var tileUpOffset = Math.floor((newPositionY + this.spriteFeetOffsetUp) / 32)
			var tileDownOffset = Math.floor((newPositionY + this.spriteFeetOffsetDown) / 32)
			var tileRightOffset = Math.floor((newPositionX + this.spriteFeetOffsetRight)  / 32);

			var tileUp = this.map.map[tileRightOffset][tileUpOffset];
			var tileDown = this.map.map[tileRightOffset][tileDownOffset];

			// Check level object collision
			for (i in this.levelObjects) {
				var levelObject = this.levelObjects[i];
				if ((levelObject.x == tileRightOffset && levelObject.y == tileUpOffset) ||
					(levelObject.x == tileRightOffset && levelObject.y == tileDownOffset))
					passable = levelObject.playerWalk(this);
			}

			if (tileUp == null || tileDown == null)
				passable = false;
			else if (tileUp.passable == false || tileDown.passable == false)
				passable = false;
		}

		else if (this.moveDirection == "char_left") {
			var tileUpOffset = Math.floor((newPositionY + this.spriteFeetOffsetUp) / 32)
			var tileDownOffset = Math.floor((newPositionY + this.spriteFeetOffsetDown) / 32)
			var tileLeftOffset = Math.floor((newPositionX + this.spriteFeetOffsetLeft)  / 32);

			var tileUp = this.map.map[tileLeftOffset][tileUpOffset];
			var tileDown = this.map.map[tileLeftOffset][tileDownOffset];

			// Check level object collision
			for (i in this.levelObjects) {
				var levelObject = this.levelObjects[i];
				if ((levelObject.x == tileLeftOffset && levelObject.y == tileUpOffset) ||
					(levelObject.x == tileLeftOffset && levelObject.y == tileDownOffset))
					passable = levelObject.playerWalk(this);
			}

			if (tileUp == null || tileDown == null)
				passable = false;
			else if (tileUp.passable == false || tileDown.passable == false)
				passable = false;
		}

		// Check map boundaries
		if (passable == false)
			return;

		this.changePosition(newPositionX / 32, newPositionY / 32);
	}

	this.init();
}

Player.prototype = Object.create(Character.prototype);
Player.prototype.constructor = Player;