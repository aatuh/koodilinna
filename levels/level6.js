
function Level6(game, spikeTrap, endObject, TriggerTile, BlockObject) {
	var _this = this;
	
	this.init = function() {
		this.map = {"map":[[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":24,"drawTileY":7,"passable":false},{"drawTileX":24,"drawTileY":8,"passable":false},{"drawTileX":24,"drawTileY":9,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":24,"drawTileY":6,"passable":false},{"drawTileX":25,"drawTileY":5,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":25,"drawTileY":7,"passable":false},{"drawTileX":25,"drawTileY":8,"passable":false},{"drawTileX":25,"drawTileY":9,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":25,"drawTileY":5,"passable":false},{"drawTileX":25,"drawTileY":5,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":24,"drawTileY":7,"passable":false},{"drawTileX":24,"drawTileY":8,"passable":false},{"drawTileX":24,"drawTileY":9,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":25,"drawTileY":5,"passable":false},{"drawTileX":25,"drawTileY":5,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":25,"drawTileY":7,"passable":false},{"drawTileX":25,"drawTileY":8,"passable":false},{"drawTileX":25,"drawTileY":9,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":25,"drawTileY":6,"passable":false},{"drawTileX":25,"drawTileY":5,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":16,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":5,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":3,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":3,"passable":false},{"drawTileX":17,"drawTileY":3,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":17,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":15,"drawTileY":3,"passable":true},{"drawTileX":15,"drawTileY":3,"passable":true},{"drawTileX":15,"drawTileY":3,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":5,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":16,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":15,"drawTileY":3,"passable":true},{"drawTileX":15,"drawTileY":3,"passable":true},{"drawTileX":15,"drawTileY":3,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":17,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":5,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":16,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":5,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":17,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":16,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":5,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":17,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":18,"drawTileY":36,"passable":false},{"drawTileX":18,"drawTileY":37,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":false},{"drawTileX":11,"drawTileY":5,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":16,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":3,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":19,"drawTileY":36,"passable":false},{"drawTileX":19,"drawTileY":37,"passable":false},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":11,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":16,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":17,"drawTileY":2,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":16,"drawTileY":4,"passable":false},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":13,"drawTileY":4,"passable":true},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},{"drawTileX":17,"drawTileY":4,"passable":false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"top":[[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":27,"drawTileY":49},{"drawTileX":29,"drawTileY":49},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":24,"drawTileY":52},{"drawTileX":24,"drawTileY":53},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":11,"drawTileY":48},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":25,"drawTileY":52},{"drawTileX":25,"drawTileY":53},null,null,null,null,null,null,null,null,null,null,{"drawTileX":27,"drawTileY":50},{"drawTileX":27,"drawTileY":51},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":22,"drawTileY":50},null,{"drawTileX":11,"drawTileY":52},{"drawTileX":11,"drawTileY":53},null,null,null,{"drawTileX":8,"drawTileY":40},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":27,"drawTileY":50},{"drawTileX":27,"drawTileY":51},null,null,null,null,null,null,null,null,null,null,{"drawTileX":12,"drawTileY":52},{"drawTileX":12,"drawTileY":53},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":21,"drawTileY":50},{"drawTileX":10,"drawTileY":37},null,null,{"drawTileX":22,"drawTileY":50},null,{"drawTileX":13,"drawTileY":52},{"drawTileX":13,"drawTileY":53},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":26,"drawTileY":52},{"drawTileX":26,"drawTileY":53},null,null,null,null,null,{"drawTileX":22,"drawTileY":51},null,null,null,null,null,null,null,null,null,{"drawTileX":15,"drawTileY":54},{"drawTileX":15,"drawTileY":55},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"drawTileX":27,"drawTileY":52},{"drawTileX":27,"drawTileY":53},null,null,null,null,null,null,null,{"drawTileX":30,"drawTileY":52},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]]}
		this.game = game;
		this.spikeTrap = spikeTrap;
		this.endObject = endObject;
		this.triggerTile = TriggerTile
		this.blockObject = BlockObject

		this.levelObjects = [];

		this.playerStartX = 2;
		this.playerStartY = 24;

		this.playerFacing = "right";

		this.mainQuest = new Level6Main(this);
		this.magicWordsQuest = new Level6MagicWords(this);
		this.cleanUpQuest = new Level6Cleanup(this);

		// Set editor's line click callback
		clickCallback = this.editorClick

		this.startText = hereDoc(function() {/*!
<div>
<h3>Kenttä 6 - Noita</h3>

Pääsitpähän ulos kammiosta, etkä joutunut jäämään sinne ikuisiksi ajoiksi. Olet nyt linnan kuluneessa
ja nuhraantuneessa osassa, jossa ei kuulu kuin tuulen ulinaa. Hetkonen, jostain kuuluu kylläkin kummaa
mutinaa...

<h3>Mitä taikoja opit tässä kentässä?</h3>

Tällä kertaa vuorossa ovat <b>listat</b>. Niillä voidaan nimensä mukaisesti listata eri asioita, kuten
ostoksia tai kouluarvosanoja.

</div>
*/});

		this.endStagingCode = hereDoc(function() {/*!
			var nettisivut, videosivusto;
*/})
		this.endFinalizingCode = hereDoc(function() {/*!
			if (nettisivut != undefined)
				sano("Joitakin nettisivuja on " + nettisivut);

			if (videosivusto != undefined)
				sano("Katselen usein videoita palvelusta nimeltään " + videosivusto);
*/})

		this.endText = hereDoc(function() {/*!
<div>
<h3>Kenttä läpäisty!</h3>

<div>
Noita muuttui lumiukoksi ja pääset nyt etenemään linnassa ilman pelkoa, että joku muuttaa sinut limaiseksi hyönteiseksi.
</div>

<h3>Mitä taikoja opit tässä kentässä?</h3>

<div>
Tämä kenttä esille <b>listat</b>. Listoihin voidaan laittaa asioita ja ottaa niitä pois. While-taikasanalla
listoja voidaan käydä nopeasti läpi.
</div>

<div class="codeDiv">
<textarea>
nettisivut = ["Youtube", "Iltalehti"] // Lista seurattavista nettisivuista

nettisivut.push("Facebook") // Lisää nettisivuihin Facebook
nettisivut.splice(1,1) // Otetaan Iltalehti pois

videosivusto = nettisivut[0] // Katsotaan mikä on eka kohta listassa. Listoissahan ensimmäinen kohta on "nollas" kohta
</textarea>
</div>

</div>
*/});

		// Staging code to be run before editor code
		this.stagingCode = "";

		// Code to be shown in the editor
		this.editorCode = "";

		this.finalizingCode = "";

		// Keep the original code safe
		this.editedCode = this.editorCode;
	}

	this.drawObjects = function() {

		this.witch = new Npc(this.game, "witch.png", 8, 38,
			"Auta minua muistamaan tämän kirotun taian viimeinen ainesosa.",
			"En muista eliksiirini viimeistä ainesosaa. Auta minua nuorella järjenjuoksullasi.",
			undefined, "#6d5b90", "Noita", this.mainQuest
		);

		this.levelObjects.push(this.witch);
		this.witch.changeFacing("left");

		// End zones
		var opts = {
			src: "tileset.png",
			x: 14,
			y: 25,
			tileX: 0,
			tileY: 1,
			visible: false,
			endable: false,
			noEndText: "Olen sen verran utelias, että haluaisin ottaa selvää mutisijan alkuperästä ennen matkan jatkamista."
		};

		this.end1 = new this.endObject(this.game, opts);
		this.levelObjects.push(this.end1);

		opts.y = 26;
		this.end2 = new this.endObject(this.game, opts);
		this.levelObjects.push(this.end2);

		// Snowman
		var opts = {
			src: "tileset.png",
			x: 9,
			y: 39,
			tileX: 8,
			tileY: 33,
			visible: false,
			endable: false,
			noEndText: "Ilkeä noita sai hieman viilennystä.",
			reacting: false
		};

		this.snowman = new this.endObject(this.game, opts);
		this.levelObjects.push(this.snowman);

		// Trigger to activate start dialog
		var opts = {
			src: "tileset.png",
			x: 3,
			y: 25,
			tileX: 2,
			tileY: 0,
			visible: false,
			triggerCallback: this.startDialog
		};

		var trigger = new TriggerTile(this.game, opts);
		this.levelObjects.push(trigger);

		// Trigger to activate main quest
		var opts = {
			src: "tileset.png",
			x: 5,
			y: 36,
			tileX: 2,
			tileY: 0,
			visible: false,
			triggerCallback: this.mainQuest.activateQuest
		};

		this.trigger1 = new TriggerTile(this.game, opts);
		this.levelObjects.push(this.trigger1);

		opts.x = 6;
		this.trigger2 = new TriggerTile(this.game, opts);
		this.levelObjects.push(this.trigger2);

		// Trigger to give missing ingredient hint
		var opts = {
			src: "tileset.png",
			x: 12,
			y: 32,
			tileX: 2,
			tileY: 0,
			visible: false,
			triggerCallback: this.mainQuest.giveHint
		};

		trigger = new TriggerTile(this.game, opts);
		this.levelObjects.push(trigger);

		// End zones
		var opts = {
			src: "tileset.png",
			x: 12,
			y: 32,
			tileX: 0,
			tileY: 1,
			visible: false,
			endable: false,
			reacting: true,
			noEndText: ""
		};

		paperEnd = new this.endObject(this.game, opts);
		this.levelObjects.push(paperEnd);
	}
	
	this.getQuestData = function(questId) {
		if (this.cleanUpQuest.questId == questId)
			return this.cleanUpQuest;
			
		else if (this.magicWordsQuest.questId == questId)
			return this.magicWordsQuest;
			
		else if (this.mainQuest.questId == questId)
			return this.mainQuest;
	}

	this.runCode = function(code, quest) {
		if (quest == undefined) {
			eval(this.endStagingCode);
			eval(code);
			eval(this.endFinalizingCode);
			return;
		}

		this.stagingCode = quest.stagingCode;
		this.editedCode = code;
		this.finalizingCode = quest.finalizingCode;

		// Set up the staging code
		eval(this.stagingCode);

		// Run the actual editor code
		var errorFree = true;
		try {
			eval(this.editedCode);
		}
		catch (e) {
			errorFree = false;
			this.game.showCodeError(e.lineNumber, e.message);
		}

		// Run the finalizing code
		if (errorFree == true)
			eval(this.finalizingCode);
	}

	this.getLevelObjects = function() {
		return this.levelObjects;
	}

	// Show help text when a line from the code editor is clicked
	this.editorClick = function(line) {
		var helpText = _this.codeHelp[line];

		if (helpText != undefined)
			codeHelp.innerHTML = _this.codeHelp[line];

		// Inform spellbook about click
		_this.game.lineHintClicked()
	}

	this.removeLevelObject = function(levelObject) {
		for (i in this.levelObjects) {
			var object = this.levelObjects[i];

			if (object === levelObject) {
				this.levelObjects.splice(i, 1);

				// NPCs require removing their special NPC tiles too
				if (levelObject instanceof Npc == true) {
					this.removeLevelObject(levelObject.npcBlock1);
					this.removeLevelObject(levelObject.npcBlock2);
				}
					
			}
		}
	}

	//
	// Level specific functions
	//
	this.startDialog = function() {
		if (_this.startDialogTold == true)
			return;

		_this.startDialogTold = true;

		_this.game.createSpeech(_this.game.player, "Tämä osa linnasta näyttää olleen autiona ties kuinka kauan...");
		_this.game.addSpeech(_this.game.player, "Yllätys kyllä, kuulen jostakin hiljaista mutinaa.");
	}

	this.questsDone = function() {
		this.game.addSpeech(this.game.player, "Noniin, olen tehnyt sinulle kaikenlaista. Joko minä NYT voin mennä?");
		this.game.addSpeech(this.witch, "Sinä et mene yhtään minnekään. Jäät tänne orjakseni ainakin seuraavaksi 300 vuodeksi.");
		this.game.addSpeech(this.game.player, "Ei minulla ole niin paljon aikaa, minä häivyn nyt!");
		this.game.addSpeech(this.witch, "Taion sinut kahleisiin. Parasta alkaa totuttelemaan niihin!");
		this.game.addSpeech(this.witch, "...");
		this.game.addSpeech(this.witch, "Miten se loitsu menikään? Minun pitäisi muistaa se nyt! Miksi en muista sitä?");
		this.game.addSpeech(this.witch, "Voi ei, nyt tiedän miksi. Sekoitin tahallani loitsukirjani loitsuja hämätäkseni vastustajiani!");
		this.game.addSpeech(this.witch, "En muistanut koko typerää asiaa. Annoit minulle jonkin toisen eliksiirin ainesosat!", this.removeWitchSequence);
	}

	this.removeWitchSequence = function() {
		$("#spellBookContainer").hide();
		_this.game.flashColor("white", "slow", _this.removeWitch);
	}

	this.removeWitch = function() {
		_this.game.createSpeech(_this.game.player, "Jaa, eliksiiri taisi muuttaa noidan lumiukoksi... vai onko kyseessä lumiakka...?");
		_this.game.addSpeech(_this.game.player, "No, ainakin taidan olla vapaa lähtemään.");

		_this.snowman.setVisible(true);
		_this.snowman.reacting = true;
		_this.removeLevelObject(_this.witch);
		_this.game.removeNpc(_this.witch.sprite);

		_this.end1.endable = true;
		_this.end2.endable = true;
	}

	this.init();
}

Level6.quests = ["Level6Main", "Level6MagicWords", "Level6Cleanup"];

function Level6MagicWords(level) {
	var _this = this;

	this.init = function() {
		this.level = level;

		this.questActivated = false;
		this.questFinished = false;
		this.questId = "Level6MagicWords";
		this.questTitle = "Lausu noidan taikasanat";
		this.questDescription = hereDoc(function() {/*!
Noita haluaa parantaa muistieliksiiriään ja haluaa sinun lausuvan taikasanat sitä varten.
<br /><br />
Kirjoita koodi. koka käy annetun <b>loitsu</b>-listan sananparret läpi <b>while</b>-taikasanan avulla
ja lausuu ne. Laita kukin sananparsi vuorollaan muuttujaan nimeltä <b>sananparsi</b> ja kirjoita sen
jälkeen taikasana <b>lausuSananparsi()</b>, jolla lausut sananparren.
*/});

		this.spellCount = 0;

		this.stagingCode = hereDoc(function() {/*!
		var loitsu, loitsunPituus, loitsuLaskuri;
		var confirmSpell = [];
		var silent = false;

		function lausuSananparsi() {
			if (silent == undefined)
				silent = false;

			if (_this.magicWordsQuest.spellCount == 0 && silent == false)
				_this.game.createSpeech(_this.game.player, sananparsi);
			else if (silent == false)
				_this.game.addSpeech(_this.game.player, sananparsi);

			_this.magicWordsQuest.spellCount += 1;
			confirmSpell.push(sananparsi);
		}
		*/});

		this.editorCode = hereDoc(function() {/*!
loitsu = ["Eliksiiri purppura.", "Sanat nää vastaan ota.", "Mä nuori suhun upotan.", "Muistin ikuisen, rajattoman."] // Tässä on lista loitsun sananparsista

sananparsi = loitsu[0] // Ota loitsusta ensimmäinen sananparsi. Laita tämä ja lausuminen while-taikasanaan
lausuSananparsi(); // Lausu sananparsi. Laita tämä while-taikasanaan <b>sananparsi</b>-muuttujan kanssa
*/});

		// Code to be run after editor code
		this.finalizingCode = hereDoc(function() {/*!
			if (loitsu == undefined) {
				this.game.addSpeech(this.witch, "Poistit loitsun kokonaan!");
				this.game.addSpeech(this.game.player, "Hups, joo. Laitanpas <b>loitsu</b>-listan takaisin.");
			}
			else if (loitsu.length < 4)
				this.game.addSpeech(this.witch, "Loitsun pituus on väärin. Sen täytyy olla neljä sananpartta, neljä.");

			else if (this.magicWordsQuest.spellCount < 4) {
				this.game.addSpeech(this.witch, "Et lausunut kaikkia neljää sananpartta!");
				this.game.addSpeech(this.game.player, "Siltä näyttää. Voisin kokeilla uudelleen eri tavalla.");
			}
			else {
				var passed = true;

				if (loitsu.toString() != confirmSpell.toString()) {
					this.game.addSpeech(this.witch, "Et lausunut alkuperäisiä antamiani sananparsia!");
				}

				// Confirm that while-loop was used
				var newCode = code.replace("loitsu", "x");
				loitsu = [1,2,3,4,5,6,7,8,9,10];
				confirmSpell = [];
				silent = true;

				eval(newCode);

				if (loitsu.toString() != confirmSpell.toString()) {
					this.game.addSpeech(this.witch, "Muuten hyvä, muttet tainnut käyttää <b>while</b>-taikasanaa. Lausu sananparret sitä käyttäen!");
					passed = false;
				}

				if (passed == true) {
					this.game.addSpeech(this.witch, "Hyvä, hyvä. Eliksiirini sai heti tummemman sävyn. Taidan juoda sen heti. Pohjanmaan kautta!");
					this.game.addSpeech(this.witch, "En ainakaan vielä tunne mitään, mutta eiköhän se siitä.");
					this.magicWordsQuest.finishQuest();
				}
			}
			this.magicWordsQuest.spellCount = 0;
		*/});

		this.editedCode = "";
		this.codeEdited = false;
		this.questElement = $("<div><p>" + this.questDescription + "</p><div class='codeDiv'><textarea>\n" + this.editorCode + "</textarea></div></div>");
	}

	this.activateQuest = function() {
		if (this.questActivated == false) {
			this.questActivated = true;
			this.level.game.displayQuest(this);

			this.level.witch.quest = this;

			this.level.game.createSpeech(this.level.witch, "Saat myös auttaa minua muistieliksiirini mahdin parantamisessa.");
			this.level.game.addSpeech(this.level.witch, "Lausu taikasanat, jotka annan sinulle. Itse en voi lausumista tehdä, koska lausujan täytyy olla hyvämuistinen.");
			this.level.game.addSpeech(this.level.game.player, "Hyvä on, mutta saat luvan päästää minut menemään kaiken tämän jälkeen.");
			this.level.game.addSpeech(this.level.witch, "Sen näemme sitten, kultaseni.");
		}
	}

	this.finishQuest = function() {
		_this.questActivated = false;
		_this.questFinished = true;
		_this.level.game.displayFinishQuest(_this);

		_this.level.questsDone();
	}

	this.init();
}

function Level6Cleanup(level) {
	var _this = this;

	this.init = function() {
		this.level = level;

		this.questActivated = false;
		this.questFinished = false;
		this.questId = "Level6Cleanup";
		this.questTitle = "Siivoa noidan varasto";
		this.questDescription = hereDoc(function() {/*!
Noita ei päästänytkään sinua menemään ja haluaa sinun siivoavan varastostaan hämähäkit. Pitkin
lattioita kipittävät hämyrit eivät taida olla kivoja noidan nukkuessa.
<br /><br />
Käytä siivoamiseen <b>splice</b>-taikasanaa. Jos osaat, voit yrittää poistaa hämähäkit kirjoittamalla
taikasanan vain kolme kertaa tai käyttää while-taikasanaa putsaamiseen!
*/});

		this.spliceCount = 0;

		this.stagingCode = hereDoc(function() {/*!
			var noidanVarasto;
*/});

		this.editorCode = hereDoc(function() {/*!
noidanVarasto = ["hämähäkki", "taikajuoma", "hämähäkki", "hämähäkki", "valkokärpässieni", "hämähäkki"] // Noidan varaston sisältö

noidanVarasto.splice(1, 1) // Poistetaan malliksi toinen asia, joka varastossa on
*/});

		// Code to be run after editor code
		this.finalizingCode = hereDoc(function() {/*!

			if (noidanVarasto == undefined) {
				this.game.createSpeech(this.witch, "Poistit varastoni kokonaan! Loihdi se heti takaisin!");
				this.game.addSpeech(this.game.player, "Hups, parasta laittaa muuttuja <b>noidanVarasto</b> takaisin.");
			}
			else {
				this.game.createSpeech(this.witch, "Varastossa on nämä asiat: " + noidanVarasto);

				// Count the number of spiders in the list
				var spiders = 0;
				for (var i in noidanVarasto) {
					if (noidanVarasto[i] == "hämähäkki")
						spiders += 1;
				}

				if (spiders != 0)
					this.game.addSpeech(this.witch, "Et siivonnut kaikkia hämähäkkejä!");

				else if (noidanVarasto.indexOf("taikajuoma") == -1 || noidanVarasto.indexOf("valkokärpässieni") == -1)
					this.game.createSpeech(this.witch, "Jätä varastosa alun perin ollut taikajuoma ja valkokärpässieni rauhaan!");

				else {
					// Check that splice() was used
					var newCode = code.replace("noidanVarasto", "x");
					var noidanVarasto = ["hämähäkki", "taikajuoma", "hämähäkki", "hämähäkki", "valkokärpässieni", "hämähäkki"];
					eval(newCode);

					if (noidanVarasto.length == 2) {
						this.game.createSpeech(this.witch, "Siivosit varaston ihanan ällöttävistä hämähäkeistä. Sillä lailla kultaseni.");
						this.cleanUpQuest.finishQuest();
					}
					else
						this.game.addSpeech(this.game.player, "Otin varastosta hämähäkit pois, mutta en käyttänyt <b>splice</b>-taikasanaa!");
				}
			}
		*/});

		this.editedCode = "";
		this.codeEdited = false;
		this.questElement = $("<div><p>" + this.questDescription + "</p><div class='codeDiv'><textarea>\n" + this.editorCode + "</textarea></div></div>");
	}

	this.activateQuest = function() {
		if (this.questActivated == false) {
			this.questActivated = true;
			this.level.game.displayQuest(this);

			this.level.witch.quest = this;

			this.level.game.addSpeech(this.level.witch, "Eipäs kiirehditä minnekään. Kun kerta olet noin avulias, voisit saman tien myös siivota hämähäkit varastostani.");
			this.level.game.addSpeech(this.level.witch, "Ja muistanet, että kesken et lähde minnekään, tai muutan sinut iljetykseksi!");
			this.level.game.addSpeech(this.level.game.player, "-.-");
		}
	}

	this.finishQuest = function() {
		_this.questActivated = false;
		_this.questFinished = true;
		_this.level.game.displayFinishQuest(_this);

		_this.level.magicWordsQuest.activateQuest();
	}

	this.init();
}

function Level6Main(level) {
	var _this = this;

	this.init = function() {
		this.level = level;

		this.questActivated = false;
		this.questFinished = false;
		this.questId = "Level6Main";
		this.questTitle = "Keksi muistieliksiirin ainesosa";
		this.questDescription = hereDoc(function() {/*!
Noita haluaa sinun auttavan muistieliksiirinsä ainesosan muistamisessa.
<br /><br />
Käytä tehtävän ratkaisemisessa push-taikasanaa.
*/});

		this.stagingCode = hereDoc(function() {/*!
			var ainesosat;
*/});

		this.editorCode = hereDoc(function() {/*!
ainesosat = ["kalkkarouute", "madonnivel", "rautajauhe", "viherhappo"] // Tässä on lista eliksiirin ainesosista
*/});

		// Code to be run after editor code
		this.finalizingCode = hereDoc(function() {/*!

			if (ainesosat == undefined) {
				this.game.createSpeech(this.witch, "Eihän tässä ole ainesosia ollenkaan! Laita <b>ainesosat</b>-lista takaisin koodiin.");
			}

			else {
				var missingIngredient = "vaniljajauhe";
				var missingFound = false;

				var originals = 0;
				var ingredientsText = "";

				extraIngredients = [];

				for (i in ainesosat) {
					var aines = ainesosat[i];

					if (aines == "kalkkarouute" || aines == "madonnivel" || aines == "rautajauhe" || aines == "viherhappo") {
						originals += 1;
					}
					else if (aines == missingIngredient)
						missingFound = true;
					else
						extraIngredients.push(aines);

					if (i == ainesosat.length - 2)
						ingredientsText += aines + " ja ";
					else if (i == ainesosat.length - 1)
						ingredientsText += aines;
					else
						ingredientsText += aines + ", ";
				}

				this.game.createSpeech(this.game.player, "Nyt meillä on seuraavat ainekset: " + ingredientsText);


				if (originals != 4) {
					this.game.addSpeech(this.witch, "Hetkonen, siitähän puuttuu jokin alkuperäisistä aineksista. Pidä ainakin ne listassa.");
				}

				else if (missingFound == true) {
					// Check that push() was used
					code = code.replace("ainesosat", "x");
					ainesosast = [];
					eval(code);

					var passed = true
					if (ainesosat[0] != "vaniljajauhe")
						passed = false;

					this.game.addSpeech(this.witch, "Kyllä! Nyt muistan sen viimein! Puuttuva aines oli todellakin " + missingIngredient + "!");

					if (passed == true)
						this.game.addSpeech(this.game.player, "Ainesosa on siis oikein, mutta minun täytyy käyttää <b>push</b>-taikasanaa ainesosan lisäämiseksi listaan.");

					else {
						this.game.addSpeech(this.game.player, "Hienoa, että muistit. Saanko minä nyt mennä?");
						this.mainQuest.finishQuest();
						this.cleanUpQuest.activateQuest();
					}			
				}
				else {
					ingredientsText = "";

					if (extraIngredients.length == 1)
						ingredientsText = extraIngredients[0];
					else {
						for (i in extraIngredients) {
							var aines = extraIngredients[i];

							if (i == extraIngredients.length - 2)
								ingredientsText += aines + " ja ";
							else if (i == extraIngredients.length - 1)
								ingredientsText += aines;
							else
								ingredientsText += aines + ", ";
						}
					}

					this.game.addSpeech(this.witch, "Hmmm... sekoitanpas äkkiä uuden aineen keittämääni liuokseen.");

					if (extraIngredients.length == 0) {
						this.game.addSpeech(this.witch, "Yritätkö huijata minua? Et lisännyt ainesosiin mitään!");
						this.game.addSpeech(this.game.player, "Voisin yrittää käyttää <b>push</b>-taikasanaa asioiden lisäämisessä listaan.");
					}

					else {
						if (extraIngredients.length == 1)
							this.game.addSpeech(this.witch, "Oi, minkä ihanan karvaan ja rujon aromin " + ingredientsText + " tuo.");
						else
							this.game.addSpeech(this.witch, "Oi, minkä ihanan karvaan ja rujon aromin lisäämäsi " + ingredientsText + " tuovat.");

						this.game.addSpeech(this.witch, "Tämä ei kuitenkaan virkistä muistiani, joten ainesosa on väärä.");
						this.game.addSpeech(this.witch, "Mieleeni juolahti, että resepti oli minulla paperilla ja olen luultavasti pudottanut sen jonnekin. Voit yrittää etsiä sitä.");
					}
				}
			}
		*/});

		this.editedCode = "";
		this.codeEdited = false;
		this.questElement = $("<div><p>" + this.questDescription + "</p><div class='codeDiv'><textarea>\n" + this.editorCode + "</textarea></div></div>");
	}

	this.giveHint = function() {
		_this.level.game.createSpeech(_this.level.game.player, "Läjä sekalaisen oloisia, ajan hampaissa hapristuneita papereita.");
		_this.level.game.addSpeech(_this.level.game.player, "Eräässä lukee vanhahtavalla käsialalla 'Elicksiiri wanhan muistin hyödyx'");
		_this.level.game.addSpeech(_this.level.game.player, "'Ainexina: kalkkarouute, madonnivel, rautajauhe, viherhappo, vaniljajauhe'");

		if (_this.questActivated == false)
			_this.level.game.addSpeech(_this.level.game.player, "Melkoista hömppää. Modernit taikojentekijät saavat tuollaisesta enintään mahanpuruja.");
		else
			_this.level.game.addSpeech(_this.level.game.player, "Taisin löytää noidan eliksiiristä puuttuvan ainesosan, vaniljajauheen.");
	}

	this.activateQuest = function() {
		if (_this.questActivated == false) {
			_this.questActivated = true;
			_this.level.game.displayQuest(_this);

			_this.level.trigger1.triggerCallback = function() {}
			_this.level.trigger2.triggerCallback = function() {}

			_this.level.game.createSpeech(_this.level.witch, "Se on sittenkin koirankarava. Ei voi olla, kokeilin sitä vasta. RAAH! Ehkä kuitenkin hopeajauhe...");
			_this.level.game.addSpeech(_this.level.witch, "Ahaa, kutsumattomia vieraita. Millä luvalla sinä olet täällä? Haluatko, että muutan sinut Muk-nilviäiseksi?");
			_this.level.game.addSpeech(_this.level.witch, "Hmmm... ehkä sinusta onkin hyötyä. Aistin sinussa vielä nuoruuden voiman ja säteilet taikuutta.");
			_this.level.game.addSpeech(_this.level.witch, "Ainiin, miten töykeää. Nimeni on Halar ja olen noita. Olen asettunut tänne linnan vanhaan ja rauhalliseen osaan.");
			_this.level.game.addSpeech(_this.level.witch, "Nuorekkaasta ulkomuodostani huolimatta olen kenties elänyt pidempään kuin moni muu. Pitkäikäisyys ei tule kuitenkaan ongelmitta.");
			_this.level.game.addSpeech(_this.level.witch, "Olen sattunut unohtamaan erään eliksiirin reseptin ainesosan. Sattumalta kyseinen litku tehostaa juuri muistia.");
			_this.level.game.addSpeech(_this.level.witch, "Saat luvan auttaa minua muistamaan reseptini viimeisen ainesosan.");
			_this.level.game.addSpeech(_this.level.witch, "Jos et suostu, muutan sinut joksikin hirvitykseksi. Sanotaanpa vaikkapa Zurp-madoksi.");

			_this.level.end1.noEndText = "En uskalla lähteä pois ilman noidan lupaa, tai hän tekee minulle jotakin kamalaa.";
			_this.level.end2.noEndText = "En uskalla lähteä pois ilman noidan lupaa, tai hän tekee minulle jotakin kamalaa.";
		}
	}

	this.finishQuest = function() {
		_this.questActivated = false;
		_this.questFinished = true;
		_this.level.game.displayFinishQuest(_this);
	}

	this.init();
}
