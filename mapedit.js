canvas = document.getElementById("gameCanvas");
canvas.width = 3500;
canvas.height = 2500;
context = canvas.getContext("2d");

passableCanvas = document.getElementById("passableCanvas");
passableCanvas.width = 2500;
passableCanvas.height = 2500;
passableContext = passableCanvas.getContext("2d");

image = new Image();
image.src = "tileset.png";

// Draw tileset
context.drawImage(
        image,
        0,
        0,
        image.width,
        image.height,
        0,
        0,
        image.width,
        image.height
);

// Draw rectangle around the map
context.beginPath();
context.rect(1085, 0, 2148, 1955);
context.lineWidth = 5;
context.strokeStyle = 'black';
context.stroke();

// Draw erase tool
context.beginPath();
context.rect(1030, 48, 32, 32);
context.fillStyle = "red";
context.fill();

selectedTileX = null;
selectedTileY = null;
eraseTool = false;

// Create map array
map = [];
mapTopLayer = [];
for (i = 0; i < 67; i++) {
        map.push(new Array(60));
        mapTopLayer.push(new Array(60));
}

drawOnTop = false;
layerCheckbox = document.getElementById("layerCheckbox")
layerCheckbox.checked = false

layerCheckbox.addEventListener('click', function(event) {
        if (drawOnTop == false)
                drawOnTop = true;
        else
                drawOnTop = false;
});

mapDrawer = new MapDrawer();

textArea = document.getElementById("textArea");
textArea.value = "";

loadButton = document.getElementById("loadButton")
loadButton.addEventListener('click', function(event) {
        mapJson = JSON.parse(textArea.value)
        mapDrawer.drawMap(mapJson, context, image);
        
        map = mapJson.map;
        mapTopLayer = mapJson.top;
});

passableCanvasVisible = false
passableButton = document.getElementById("passableButton");
passableButton.addEventListener('click', function(event) {
        if (passableCanvasVisible == true) {
                passableCanvas.style.display = "none";
                passableCanvasVisible = false;
        }
        else {
                passableCanvas.style.display = "block";
                passableCanvasVisible = true;
        }
});

function drawPassableTile(passable) {
        if (passable == undefined)
                passable = false

        passableContext.clearRect(tileX * 32, tileY * 32, 32, 32);

        if (passable == false)
                var color = "rgba(255, 0, 0, 0.5)";
        else
                var color = "rgba(0, 255, 0, 0.5)";

        passableContext.beginPath();
        passableContext.rect(tileX * 32, tileY * 32, 32, 32);
        passableContext.fillStyle = color;
        passableContext.fill();
}
mapDrawer.drawPassableTile = drawPassableTile

function addTileToMap() {
        mapX = tileX - 34;
        mapY = tileY;

        if (drawOnTop == false) {
                // Create map JSON and display it in the textbox
                coordData = {
                        drawTileX: selectedTileX,
                        drawTileY: selectedTileY,
                        passable: false
                };

                map[mapX][mapY] = coordData;
        }
        else {
                coordData = {
                        drawTileX: selectedTileX,
                        drawTileY: selectedTileY
                };

                mapTopLayer[mapX][mapY] = coordData;
        }

        generateMapString();
}

function removeTileFromMap() {
        // Clear drawed tile with white
        context.beginPath();
        context.rect(tileX * 32, tileY * 32, 32, 32);
        context.fillStyle = "white";
        context.fill();

        mapX = tileX - 34;
        mapY = tileY;

        map[mapX][mapY] = undefined;
        mapTopLayer[mapX][mapY] = undefined;

        mapData = JSON.stringify({map: map, top: mapTopLayer});
        textArea.value = mapData;
}

passableCanvas.addEventListener('mousedown', function(event) {
        var elemLeft = canvas.offsetLeft,
                elemTop = canvas.offsetTop;

        clickX = event.pageX - elemLeft;
        clickY = event.pageY - elemTop;
 
        tileX = Math.ceil(clickX / 32) - 1;
        tileY = Math.ceil(clickY / 32) - 1;

        mapX = tileX - 34;
        mapY = tileY;

        if (map[mapX] == undefined || map[mapX][mapY] == undefined)
                return;

        if (map[mapX][mapY].passable == true)
                var passable = false;
        else
                var passable = true;

        map[mapX][mapY].passable = passable;

        generateMapString();

        drawPassableTile(passable);
});

var lastTileX, lastTileY;
passableCanvas.addEventListener('mousemove', function(event) {
        //console.log(event)
        if (event.buttons == 0)
                return;

        var elemLeft = canvas.offsetLeft,
                elemTop = canvas.offsetTop;

        clickX = event.pageX - elemLeft;
        clickY = event.pageY - elemTop;
 
        tileX = Math.ceil(clickX / 32) - 1;
        tileY = Math.ceil(clickY / 32) - 1;

        if (lastTileX == tileX && lastTileY == tileY)
                return;

        lastTileX = tileX;
        lastTileY = tileY;

        mapX = tileX - 34;
        mapY = tileY;

        if (map[mapX] == undefined || map[mapX][mapY] == undefined)
                return;

        if (map[mapX][mapY].passable == true)
                var passable = false;
        else
                var passable = true;

        map[mapX][mapY].passable = passable;

        generateMapString();

        drawPassableTile(passable);
});

function generateMapString() {
        mapData = JSON.stringify({map: map, top: mapTopLayer});
        textArea.value = mapData;
}

canvas.addEventListener('mousedown', function(event) {
        var elemLeft = canvas.offsetLeft,
                elemTop = canvas.offsetTop;

        clickX = event.pageX - elemLeft;
        clickY = event.pageY - elemTop;
 
        tileX = Math.ceil(clickX / 32) - 1;
        tileY = Math.ceil(clickY / 32) - 1;

        // Check if the erase tool was selected
        if (clickX >= 1030 && clickX <= 1062 && clickY >= 48 && clickY <= 70) {
                eraseTool = true;

                context.beginPath();
                context.rect(1030, 0, 32, 32, 32);
                context.fillStyle = "red";
                context.fill();

                return;
        }

        // Select a tile
        else if (clickX < 1030) {
                eraseTool = false;

                selectedTileX = tileX;
                selectedTileY = tileY;
 
                // Clear preview tile with white
                context.beginPath();
                context.rect(1030, 0, 32, 32, 32);
                context.fillStyle = "white";
                context.fill();

                context.drawImage(
                        image,
                        selectedTileX * 32,
                        selectedTileY * 32,
                        32,
                        32,
                        1030,
                        0,
                        32,
                        32
                );
                return;
        }

        // Draw selected tile
        if (tileX > 33 && tileX <= 100 && tileY <= 60 && selectedTileX != null) {
                if (eraseTool == true) {
                        removeTileFromMap();
                        return;
                }

                mapDrawer.drawSelectedTile(context, image, drawOnTop);
                drawPassableTile();
                addTileToMap();
        }
});
 
canvas.addEventListener('mousemove', function(event) {
        if (event.buttons != 1)
                return;
 
        var elemLeft = canvas.offsetLeft,
                elemTop = canvas.offsetTop;

        clickX = event.pageX - elemLeft;
        clickY = event.pageY - elemTop;
 
        tileX = Math.ceil(clickX / 32) - 1;
        tileY = Math.ceil(clickY / 32) - 1;

        // Don't draw on tileset part and limit size
        if (clickX < 1100 || tileX < 34 || tileX > 100 || tileY > 60 || selectedTileX == null)
                return;

        if (eraseTool == true) {
                removeTileFromMap();
                return;
        }

        mapDrawer.drawSelectedTile(context, image, drawOnTop);
        drawPassableTile();
        addTileToMap();
});