
function MapDrawer() {
        this.tileX;
        this.tileY;
        this.selectedTileX;
        this.selectedTileY;
        this.drawOnTop = false;

        this.minX = undefined;
        this.maxX = undefined;
        this.minY = undefined;
        this.maxY = undefined;

        // Externally defined function
        this.drawPassableTile = undefined;

        this.drawMap = function (mapJson, context, image, offsetX) {
                // Drawing in editor requires an offset
                if (offsetX == undefined)
                        offsetX = 34;

                // Weak check to confirm it's a valid map JSON
                if (mapJson.map.length != 67) {
                        console.error("Given map not valid.");
                        return;
                }

                for (i in mapJson) {
                        data = mapJson[i];

                        if (i == "top")
                                this.drawOnTop = true;

                        for (x in data) {
                                tileX = parseInt(x) + offsetX;

                                for (y in data[x]) {
                                        tile = data[x][y];

                                        if (tile == null)
                                                continue;

                                        tileY = parseInt(y);


                                        if (this.minX == undefined || tileX * 32 < this.minX)
                                                this.minX = tileX * 32;

                                        else if (this.maxX == undefined || tileX * 32 > this.maxX)
                                                this.maxX = (tileX + 1) * 32;

                                        if (this.minY == undefined || tileY * 32 < this.minY)
                                                this.minY = tileY * 32;
                                        
                                        else if (this.maxY == undefined || tileY * 32 > this.maxY)
                                                this.maxY = tileY * 32;

                                        selectedTileX = parseInt(tile.drawTileX);
                                        selectedTileY = parseInt(tile.drawTileY);

                                        this.drawSelectedTile(context, image);

                                        if (tile.passable != undefined && this.drawPassableTile != undefined)
                                                this.drawPassableTile(tile.passable);
                                }
                        }
                }
                this.drawOnTop = false;
        }

        this.reset = function() {
                this.minX = undefined;
                this.maxX = undefined;
                this.minY = undefined;
                this.maxY = undefined;
        }

        this.drawSelectedTile = function(context, image) {
                if (this.drawOnTop == false) {
                        // Clear drawed tile with white
                        context.beginPath();
                        context.rect(tileX * 32, tileY * 32, 32, 32);
                        context.fillStyle = "white";
                        context.fill();
                }

                // Draw selected tile
                context.drawImage(
                        image,
                        selectedTileX * 32,
                        selectedTileY * 32,
                        32,
                        32,
                        tileX * 32,
                        tileY * 32,
                        32,
                        32
                );
        }
}