
function AchievementsController(game) {
	var _this = this;
	
	this.init = function() {
		this.game = game;

		var achievements = [FirstLevel]
		this.achievementsList = [];
		
		this.mainElement = $("<div id='achievements'><h3>Saavutuksesi</h3></div>");
		
		for (var i in achievements) {
			var ach = new achievements[i](this.game);
			
			var achElement = $("<div class='achievement'><img src='" + ach.image + "'><br />" + ach.fullName + "</div>");
			this.game.bindHover(achElement, ach.description);
			this.mainElement.append(achElement);

			this.achievementsList.push(ach);
		}
		
		$("#achievementButton").click(function() {
			$("#spellBook").html(_this.mainElement);
			$("#spellBookContainer").show();
		});
	}
	
	this.achieve = function(name) {
		for (var i in this.achievementsList) {
			var ach = this.achievementsList[i];

			if (ach.name == name)
				ach.achieve();
		}
	}

	this.init();
}

function Achievement(game) {
	var _this = this;

	this.init = function() {
		this.game = game;

		this.complete = false;
		this.registrees = [];
		this.updates = 0;
	}

	this.register = function(registree) {
		this.registrees.push(registree);
	}

	this.achieve = function() {
		//if (this.updates == this.registrees.length) {
			this.complete = true;
			$("#achievementButton").hide("explode").show("explode");
			this.game.createSpeech(this.game.player, "Sain saavutuksen! Sen nimi on <b>" +
				this.fullName + "</b>");
		//}
	}

	this.init();
}

function FirstLevel(game) {
	var _this = this;

	this.init = function() {
		this.image = "star.png";
		this.name = "FirstLevel";
		this.fullName = "Eka kenttä";
		this.description = "Läpäise ensimmäinen kenttä";

		Achievement.call(this, game);
	}

	this.init();
}

FirstLevel.prototype = Object.create(Achievement.prototype);
FirstLevel.prototype.constructor = FirstLevel;