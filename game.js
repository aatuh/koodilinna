﻿
(function() {
	window.requestAnimationFrame = window['mozRequestAnimationFrame']
	window.cancelAnimationFrame = window['mozCancelAnimationFrame'] || window['mozCancelRequestAnimationFrame']

}());

(function () {

	function Game(startLevel, characterGender) {
		var _this = this;

		this.init = function() {
			if (startLevel == undefined)
				startLevel = 0;
			else
				startLevel -= 1;

			if (characterGender == undefined)
				characterGender = "male";

			this.characterGender = characterGender;

			this.callbacks = {};

			var bodyWidth = document.body.offsetWidth;
			var bodyHeight = document.body.offsetHeight;

			this.gameCanvas = document.getElementById("gameCanvas");
			this.gameContext = this.gameCanvas.getContext("2d");

			this.gameCanvas.width = 1920;
			this.gameCanvas.height = 2144;

			this.mapCanvas = document.getElementById("mapLayer");
			this.mapContext = this.mapCanvas.getContext("2d");

			this.mapCanvas.width = 1920;
			this.mapCanvas.height = 2144;

			this.drawCanvas = document.getElementById("drawCanvas");
			this.drawContext = this.drawCanvas.getContext("2d");
			
			this.drawCanvas.width = bodyWidth;
			this.drawCanvas.height = bodyHeight;

			this.dialogLayer = document.getElementById("dialogLayer");

			var flashLayer = document.getElementById("flashLayer");
			flashLayer.style.width = bodyWidth;
			flashLayer.style.height = bodyHeight;

			this.animations = [];
			this.fps = 80;
			this.now = then = Date.now();
			this.then = Date.now();
			this.interval = 1000 / this.fps;
			this.delta = 0;

			this.speechQueue = [];
			this.sayOverride = false;

			this.animationsController = new AnimationsController(this);
			this.achievementsController = new AchievementsController(this);
			this.miniQuestsController = new MiniQuestController(this);
			
			this.mapDrawer = new MapDrawer();

			this.tileset = new Image();
			this.tileset.src = "tileset.png";

			// Store what line hint contains
			this.lineHint = "Kun painat koodirivejä tehtäväkoodissa, ilmestyy tähän vinkkejä ja apua niihin liittyen.";

			this.hoverTip = $("<div class='hoverTip'></div>");
			this.bindHover($("#scrollButton"), "Kadonneet sivut");
			this.bindHover($("#achievementButton"), "Saavutuksesi");
			this.bindHover($("#closeSpellBook"), "Sulje");
			
			$("#closeSpellBook").click(function() {
				$("#spellBookContainer").hide();
				_this.clearSpeechQueue();
			});

			this.levelCounter = startLevel;
			this.levels = [Level1, Level2, Level3, Level4, Level5, Level6, Level7];

			// Know which quest belongs to which level
			this.questLevels = {};

			for (i in this.levels) {
				var quests = this.levels[i].quests;
				
				for (q in quests)
					this.questLevels[quests[q]] = this.levels[i];
			}
			
			this.levelEnded = true;

			this.createSpellBookButtons();

			this.editingCode = false;

			$("body").click(function(event) {

				// Clicking code running button would mess speeches
				if (event.target.type == "submit")
					return;

				if (_this.speechOn() == true || _this.levelEnded == true)
					_this.playSpeech();
				else if (event.target.getAttribute('id') == "drawCanvas")
					$("#spellBookContainer").hide();
			});

			this.tileset.addEventListener("load", function() {
				var characterImage;
				if (characterGender == "male")
					characterImage = "character_male.png"

				else if (characterGender == "female")
					characterImage = "character_female.png"

				_this.player = new Player(_this, characterImage, characterGender);

				_this.changeLevel(_this.levels[_this.levelCounter]);

				window.addEventListener("click", function(e) { _this.player.stopMoving(); })

				$("#dialogLayer").on("mousedown", function(event) {
					event.stopPropagation();
					_this.player.setAllowMove(true);
				});

				Game.gameLoop();
			});

			// Catch keyboard events
			window.addEventListener('keydown', function(event) {

				if (_this.editingCode == true)
					return;

				event.preventDefault();

				key = event.keyCode;

				if (_this.speechOn() == true) {

					// Space bar and enter
					if (key == 32 || key == 13)
						_this.playSpeech();

					return;
				}

				// Don't allow moving while coding
				if ($("#spellBookContainer").is(':visible') == true) {

					// Escape key
					if (key == 27) {
						$("#spellBookContainer").hide();
						_this.clearSpeechQueue();
					}

					return;
				}

				// Up, left, down, right
				if (key >= 37 || key <= 40) {
					_this.player.keyPressed(key);
				}
			});


			window.addEventListener('keyup', function(event) {
				event.preventDefault();

				key = event.keyCode;

				// Up, left, down, right
				if (key >= 37 || key <= 40) {
					_this.player.keyReleased(key);
				}
			});
		}

		this.bindHover = function(element, text, callback) {
			element.hover(
				function(event) {
					var left = event.pageX + 10;
					var top = event.pageY - $("#topBar").height();

					_this.hoverTip.html(text)
					$("#dialogLayer").append(_this.hoverTip);

					_this.hoverTip.css({left: left, top: top})

					_this.hoverTip.show();
				},
				function(event) {
					$(".hoverTip").remove();
				}
			);

			element.mousemove(function(event) {
				var left = event.pageX + 10;
				var top = event.pageY - $("#topBar").height();
				
				_this.hoverTip.css({left: left, top: top})

				if (callback != undefined)
					callback(event);
			});
		}

		this.removeLevelObject = function(removed) {
			for (i in this.level.levelObjects) {
				var levelObject = this.level.levelObjects[i];
				
				if (removed === levelObject)
					this.level.levelObjects.splice(i, 1);
			}
		}

		this.flashColor = function(color, speed, fadeInCallback) {
			if (speed == undefined)
				speed = "fast";

			var flashLayer = $("#flashLayer");
			flashLayer.css({"background": color});

			flashLayer.fadeIn({duration: speed, complete: fadeInCallback}).fadeOut(speed);
		}

		this.toggleFade = function(color, speed, fadeInCallback) {
			if (speed == undefined)
				speed = "fast";

			var flashLayer = $("#flashLayer");
			flashLayer.css({"background": color});

			if (flashLayer.is(':visible'))
				flashLayer.fadeOut({duration: speed, complete: fadeInCallback});
			else
				flashLayer.fadeIn({duration: speed, complete: fadeInCallback});
		}

		this.getNpcBlock = function() {
			return NpcBlock;
		}

		this.removeNpc = function(npc) {
			this.animationsController.removeAnimation(npc);
		}

		this.runCode = function() {
			this.level.runCode();
		}

		this.restartLevel = function() {
			this.changeLevel(this.levels[this.levelCounter]);
		}

		// Running the gameloop works with static function
		Game.instance = this;
		Game.gameLoop = function() {
			
			var instance = Game.instance;
			requestAnimationFrame(Game.gameLoop);
			
			instance.now = Date.now();
			instance.delta = instance.now - instance.then;

			if (instance.delta > instance.interval) {
				instance.then = instance.now - (instance.delta % instance.interval);

				instance.gameContext.clearRect(0, 0, instance.gameCanvas.width, instance.gameCanvas.height);

				// Draw animations
				for (i = 0; i < instance.animationsController.animations.length; i += 1) {
					instance.animationsController.animations[i].update();
					instance.animationsController.animations[i].render();
				}

				// Draw player last to keep on top of NPCs
				instance.player.sprite.update();
				instance.player.sprite.render();

				// Run callbacks
				for (var key in instance.callbacks)
					instance.callbacks[key]();

				// Combine other canvases to one and cut away empty space
				instance.drawContext.drawImage(
					instance.mapCanvas,
					instance.mapDrawer.minX,
					instance.mapDrawer.minY,
					instance.mapCanvas.width,
					instance.mapCanvas.height,
					0,
					0,
					instance.drawCanvas.width,
					instance.drawCanvas.width
				)

				instance.drawContext.drawImage(
					instance.gameCanvas,
					instance.mapDrawer.minX,
					instance.mapDrawer.minY,
					instance.gameCanvas.width,
					instance.gameCanvas.height,
					0,
					0,
					instance.drawCanvas.width,
					instance.drawCanvas.width
				)
			}
		}

		// Run when some of the main spellbook buttons are clicked
		this.selectSpellBookButton = function(element) {

			if (this.currentSpellBookButton != undefined)
				$(this.currentSpellBookButton).css("background", "#C49156");

			this.currentSpellBookButton = element;
			$(element).css("background", "#F7C489");
		}

		this.displayStartText = function() {
			$("#topBar").find("button").prop('disabled', true);
			$("#closeSpellBook").hide();
				
			var text = $(this.level.startText);

			var okButton = $("<button>Selvän teki. Eteenpäin!</button>");
			okButton.click(function() { _this.startTextProceed(); });

			text.append("<br /><br />", okButton, "<br /><br />");

			$("#spellBook")
			.html(text);

			$("#spellBookContainer")
			.show();
		}

		this.startTextProceed = function() {
			$("#spellBookContainer").hide();
			this.toggleFade("black", "slow");
			
			_this.player.setAllowMove(true);
			
			$("#topBar").find("button").prop('disabled', false);
			$("#closeSpellBook").show();
		}

		this.displayEndText = function() {
			$("#topBar").find("button").prop('disabled', true);
			$("#closeSpellBook").hide();
				
			var text = $(this.level.endText);

			var okButton = $("<button>Seuraavaan kenttään!</button>");
			okButton.click(function() {
				if (_this.levelCounter == _this.levels.length) {
					_this.endGame();
				}
				else
					_this.changeLevel(_this.levels[_this.levelCounter]);
			});

			text.append("<br /><br />", okButton, "<br /><br />");

			$("#spellBook").html(text);
			$("#spellBookContainer").show();


			// Refresh editor instances in order to be shown properly
			codeDivs = text.find(".codeDiv");
			for (var i = 0; i < codeDivs.length; i += 1) {
				//var editor = $(codeDivs[i]).data("CodeMirror");
				var div = $(codeDivs[i]);
				var textElement = div.find("textarea")[0];
				var editor = this.createEditor(textElement)

				var handledCode = _this.splitComments(editor.getValue());
				var lineHints = handledCode[0];
				editor.setValue(handledCode[1]);

				editor.clearHistory();

				div.data("CodeMirror", editor);
				div.data("LineHints", lineHints);

				this.addCodeButtons(div);

				if (editor != undefined)
					editor.refresh();
			}
		}

		this.createEditor = function(element) {
		    editor = CodeMirror.fromTextArea(element, {
		        lineNumbers: true,
				mode: "javascript",
		        theme: "ambiance",
		        indentWithTabs: false,
		        readOnly: false
		    });

			editor.on("mousedown", function(event) {
				_this.player.setAllowMove(false);
			});

			editor.on("keydown", function(event) {
				var parent = $(event.getWrapperElement()).parent();
				parent.find(".resetButton").show("fade", "fast");
				
				var quest = parent.data("Quest")

				if (quest != undefined) {
					quest.editedCode = event.getValue();
					quest.codeEdited = true;
				}
			});

			editor.on("focus", function(event) {
				_this.editingCode = true;
			});

			editor.on("blur", function(event) {
				_this.editingCode = false;
			});
			
			this.bindHover($(editor.getWrapperElement()), "", function(event) {
				var editor = $(event.currentTarget).parent().data("CodeMirror");
				var line = editor.coordsChar({left: event.pageX, top: event.pageY}).line;

				var parent = $(editor.getWrapperElement()).parent();
				var lineHints = parent.data("LineHints");

				preComment = editor.getLine(line).split("//")[0];

				var found = false;
				for (var i in lineHints) {
					var hint = lineHints[i];

					if (hint[0] == preComment) {
						_this.hoverTip.html(hint[1]);
						_this.hoverTip.show();
						var found = true;
						break;
					}
				}

				if (found == false)
					_this.hoverTip.hide();
			});

		    return editor;
		}

		this.runCode = function(code) {
			// Run the actual editor code
			var errorFree = true;
			try {
				eval(code);
			}
			catch (e) {
				errorFree = false;
				this.showCodeError(e.lineNumber, e.message);
			}
		}

		this.showCodeError = function(lineNumber, errorString) {
			this.createSpeech(this.player, "Minulla on jokin virhe taiassani. Se löytyy koodista riviltä " + lineNumber + ".");

			if (errorString == "unterminated string literal")
				this.addSpeech(this.player, "Virhe tuli, koska en huomannut laittaa riville toista lainausmerkkiä!");
			else if (errorString == "expected expression, got end of script")
				this.addSpeech(this.player, "Virhe johtuu siitä, että rivillä kuuluu olla muutakin kuin mitä siinä nyt on.");
			else if (errorString == "missing ) after argument list")
				this.addSpeech(this.player, "Virhe tulee siitä, että unohdin laittaa toisen sulkeen riville!");
			else if (errorString == "missing } in compound statement")
				this.addSpeech(this.player, "Virhe johtuu puuttuvasta aaltosulkeesta, eli merkistä }");

			console.error("Eval error: " + errorString + " (line " + lineNumber + ")");
		}

		this.addCodeButtons = function(element) {

			// Define code execution button functionality
			var button1 = $("<button class='executeButton'>Kokeile koodia</button>");
			button1.click(function() {
				var parent = $(this).parent();

				var editor = parent.data("CodeMirror");
				var quest = parent.data("Quest");

				$(this).prop('disabled', true);

				var codeDone = false;
				var button = $(this);
				var interval = setInterval(function() {
					if (codeDone == true && _this.speechOn() == false) {
						button.prop('disabled', false);
						window.clearInterval(interval);
					}
				}, 100);

				// Guess that current level may run the code if no quest defined (end text)
				if (quest == undefined)
					_this.level.runCode(editor.getValue(), undefined);
				else {

					var textElement = $(parent.find("textarea")[0]);

					// Code may be leaning on staging and finalizing code vars
					if (textElement.hasClass("originalCode")) {
						quest.level.runCode(editor.getValue(), undefined, true);
					}
					else {
						quest.level.runCode(editor.getValue(), quest);
					}
				}
				var codeDone = true;
			})

			// Don't allow running code when speaking
			if (this.speechOn() == true)
				button1.prop('disabled', true);

			element.append(button1);

			// Define code reset button functionality
			button2 = $("<button class='resetButton'>Palauta koodi alkuperäiseksi</button>");
			button2.click(function(event) {
				var parent = $(this).parent();

				var editor = parent.data("CodeMirror");
				var quest = parent.data("Quest");

				var textElement = $(parent.find("textarea")[0]);

				// Textarea with class originalCode is used as reset code source
				if (textElement.hasClass("originalCode"))
					var code = textElement.text();
				else {
					var code = quest.editorCode;
					quest.codeEdited = false;
				}

				var handledCode = _this.splitComments(code);
				editor.setValue(handledCode[1]);

				editor.clearHistory();

				$(this).hide("fade", "fast");
			});

			button2.hide();
			element.append(button2);

			/*
			button3 = $("<button class='questRedo'>Haluan tehdä tehtävän uudelleen!</button>");
			button3.click(function() {
				var quest = $(this).parent().data("Quest");
				quest.questActivated = false;
				quest.questFinished = false;

				_this.displayQuest(quest);

			});
			element.append(button3);
			button3.hide();
			*/
		}

		this.displayQuest = function(quest, showPage) {

			quest.questActivated = true;

			if (quest.questFinished == true && quest.pagination == false)
				return;

			var editors = [];

			var questElement = quest.questElement.clone();

			if (quest.questFinished == true)
				questElement.prepend("<h3>Tehtävä läpäisty!</h3>");
			else
				questElement.prepend("<h3>Tehtävä</h3>");

			var textarea = questElement.find("textarea")[0];
			var editor = this.createEditor(textarea);
			editors.push(editor);

			if (quest.editedCode == "")
				var editorValue = quest.editorCode;
			else
				var editorValue = quest.editedCode;

			var handledCode = this.splitComments(editorValue);
			var lineHints = handledCode[0];
			editor.setValue(handledCode[1]);

			editor.clearHistory();

			var codediv = questElement.find(".codeDiv");
			codediv.data("CodeMirror", editor);
			codediv.data("Quest", quest);
			codediv.data("LineHints", lineHints);

			if (quest.questFinished == false)
				this.addCodeButtons(codediv);

			// Use theory pages if present
			if (quest.pagination == true) {
				var theoryPage = quest.theoryPage.clone();

				// Find divs containing textarea for code
				codedivs = theoryPage.find(".codeDiv");

				// Create code editors
				for (var i = 0; i < codedivs.length; i += 1) {
					var element = $(codedivs[i]);
					var textElement = element.find("textarea")[0];

					var editor = this.createEditor(textElement);
					editors.push(editor);

					element.data("CodeMirror", editor);
					element.data("Quest", quest);

					this.addCodeButtons(element);

					var handledCode = this.splitComments(editor.getValue());
					var lineHints = handledCode[0];
					editor.setValue(handledCode[1]);

					editor.clearHistory();

					element.data("LineHints", lineHints);
				}

				var paginatedHtml = $("<div></div>");

				paginatedHtml
				.append(theoryPage)
				.append(questElement);

				$("#spellBook").html(paginatedHtml);

				var arrowRight = $("<div class='pageArrow nextPage'><img src='arrow_right.png'></div>")
				.click(function() {
					theoryPage.hide();
					questElement.show();

					// Refresh code editor
					codedivs = questElement.find(".codeDiv");
					for (var i = 0; i < codedivs.length; i += 1)
						$(codedivs[i]).data("CodeMirror").refresh();

				});

				this.bindHover(arrowRight, "Tehtävä");

				var arrowLeft = $("<div class='pageArrow previousPage'><img src='arrow_left.png'></div>")
				.click(function() {
					theoryPage.show();
					questElement.hide();

					// Refresh code editor
					codedivs = theoryPage.find(".codeDiv");
					for (var i = 0; i < codedivs.length; i += 1)
						$(codedivs[i]).data("CodeMirror").refresh();
				});

				this.bindHover(arrowLeft, "Taikaan liittyvää tietoa");

				theoryPage.append(arrowRight);
				questElement.append(arrowLeft);

				if (showPage == 1 || showPage == undefined)	{
					theoryPage.show();
					questElement.hide();
				}
				if (showPage == 2) {
					theoryPage.hide();
					questElement.show();
				}
			}
			else
				$("#spellBook").html(questElement);

			$("#spellBookContainer").show();

			if (quest.codeEdited == true)
				$(".resetButton").show();

			for (var i in editors)
				editors[i].refresh();
		}

		this.splitComments = function(code) {
			var cleanCode = "";
			var splitLines = code.split("\n");

			var lineHints = [];
			for (var i in splitLines) {
				var line = splitLines[i];

				var commentSplit = line.split("//");
				
				// Remove trailing spaces and add newline to each split
				var trimmedCode = commentSplit[0].replace(/\s+$/g,'');
				cleanCode += trimmedCode + "\n";
				
				if (commentSplit.length != 2)
					continue;

				lineHints.push([trimmedCode, commentSplit[1]]);
			}
			
			return [lineHints, cleanCode];
		}

		this.displayFinishQuest = function(quest) {
			$("#spellBookContainer").effect("highlight", {color: "#008800"}, "fast");

			$("#spellBook").find("button").hide();
			$("#spellBook").append("<br />");

			var text = $("#spellBook:visible").find("h3").html("Tehtävä läpäisty!");
		}

		this.showLesson = function(elementId, scrollTo) {

			$("#spellBook").find(".spellBookContent:visible").hide();
			$("#codeHelp").show();

			// Hide any visible lessons
			var codeHelpText = $("#codeHelpText");
			codeHelpText.find("div:visible:first").hide();

			// Display the the new lesson
			var existing = codeHelpText.find("#"+elementId);
			existing.show();

			// Refresh editor instances in order to be shown properly
			codeDivs = existing.find(".codeDiv");
			for (var i = 0; i < codeDivs.length; i += 1) {
				var editor = $(codeDivs[i]).data("CodeMirror");

				if (editor != undefined)
					editor.refresh();
			}

			// Scroll to the given element (probably a quest)
			if (scrollTo != undefined)
				document.getElementById(scrollTo).scrollIntoView(false);

		}

		/*
		this.createLesson = function(sourceElement, targetElement) {
			if (targetElement == undefined) {
				var targetElement = $("#codeHelpText");
				sourceElement.detach()
				sourceElement.hide();
			}
			targetElement.append(sourceElement);

			// Find divs containing textarea for code
			codedivs = sourceElement.find(".codeDiv");

			// Go through divs and add the code editor and the buttons
			for (var i = 0; i < codedivs.length; i += 1) {
				var element = $(codedivs[i]);
				var textElement = element.children()[0];

				// If the element is brand new without editor element
				if (element.hasClass("created") == false) {
					element.addClass("created");

					var editor = this.createEditor(textElement);

					var editorElement = $(editor.getWrapperElement());
					editorElement.addClass("questContents");

					if (element.hasClass("noButtons") == false) {

						// Define code execution button functionality
						var button1 = $("<button class='questContents'>Suorita koodi</button>");
						button1.click(function() {
							var parent = $(this).parent();

							// Get the CodeMirror instance and run its code
							var codeMirror = parent.data("CodeMirror");

							if (parent.hasClass("quest")) {
								var quest = parent.data("Quest");
								quest.level.runCode(codeMirror.getValue(), quest);
							}
							else
								_this.runCode(codeMirror.getValue());
						})
						element.append(button1);

						// Define code reset button functionality
						button2 = $("<button class='questContents'>Palauta koodi alkuperäiseksi</button>");
						button2.click(function() {
							// Get the CodeMirror instance and set its value to the original code
							var codeMirror = $(this).parent().data("CodeMirror");
							codeMirror.setValue($(this).parent().data("OriginalCode"));
						});
						element.append(button2);
					}
				}

				// Quest code needs special attention
				if (element.hasClass("quest")) {

					// Get the quest instance and set editor code
					var elementId = element.attr("id")
					var quest = this.level.getQuestData(elementId);
					
					// If not current level's quest
					if (quest == undefined) {

						// Quest may be from previously played level
						quest = element.data("Quest");

						// In this case quest's level has not been played
						if (quest == undefined) {
							
							var tempLevel = new this.questLevels[elementId]();
							quest = tempLevel.getQuestData(elementId);
						}

						var title = $("<h3>Tehtävä: " + quest.questTitle + "</h3>");

						if (quest.questFinished == true)
							var info = $("<div class='questInfo'>Olet päässyt tämän tehtävän läpi aiemmassa kentässä!</div>");
						else
							var info = $("<div class='questInfo'>Tämä tehtävä kuuluu aiempaan kenttään!</div>");

						element
						.empty()
						.append(title)
						.append(info);

						element.removeData("CodeMirror");
						element.removeData("Quest");
						element.removeData("OriginalCode");
					}
					else {
						editor.setValue(quest.editorCode);

						// Define quest redo button functionality
						button3 = $("<button class='questRedo'>Haluan tehdä tehtävän uudelleen!</button>");
						button3.click(function() {
							_this.displayQuest($(this).parent().data("Quest").questId)

						});
						element.append(button3);
						button3.hide();

						var info = $("<div class='questInfo questFinished'><b>Olet päässyt tämän tehtävän läpi! Jos haluat tehdä tehtävän uudelleen, paina alla olevaa nappia.</b></div>");
						element.prepend(info);
						info.hide();

						var info = $("<div class='questInfo'>Tätä tehtävää ei ole vielä aktivoitu! Koita löytää tehtävän antava hahmo kentästä.</div>");
						element.prepend(info);

						// Render quest description
						var description = $("<div class='questContents'>" + quest.questDescription + "</div>");
						element.prepend(description);

						// Render the quest title
						var title = $("<h3>Tehtävä: " + quest.questTitle + "</h3>");
						element.prepend(title);

						// Save data on the parent element
						element.data("OriginalCode", quest.editorCode);
						element.data("Quest", quest);

						// If quest has not been activated yet hide most of the quest contents
						if (quest.questActivated == false && quest.questFinished == false) {
							description.hide();
							editorElement.hide();
							button1.hide();
							button2.hide();
							button3.hide();
						}
						else if (quest.questFinished == true) {
							description.hide();
							editorElement.hide();
							button1.hide();
							button2.hide();
							button3.show();
						}
					}
				}
				else
					element.data("OriginalCode", textElement.innerHTML);

				// Save codemirror instance in the parent element
				element.data("CodeMirror", editor);
			}
		}
		*/

		this.createSpellBookButtons = function() {

			//
			// Create lesson buttons
			//
			this.codeButton = $("<button class='spellBookButton'>Mitä koodaus on?</button>")
			.click(function() { _this.showLesson("codeLessonText"); })
			.hide();

			this.variableButton = $("<button class='spellBookButton'>Muuttujat</button>")
			.click(function() { _this.showLesson("variableLessonText"); })
			.hide();

			this.stringButton= $("<button class='spellBookButton'>Kirjaimet</button>")
			.click(function() { _this.showLesson("stringLessonText"); })
			.hide();

			this.intButton = $("<button class='spellBookButton'>Numerot</button>")
			.click(function() { _this.showLesson("intLessonText"); })
			.hide();

			this.floatButton = $("<button class='spellBookButton'>Desimaalit</button>")
			.click(function() { _this.showLesson("floatLessonText"); })
			.hide();

			this.operatorsButton = $("<button class='spellBookButton'>Laskutoimitukset</button>")
			.click(function() { _this.showLesson("operatorsLessonText"); })
			.hide();

			this.ifButton = $("<button class='spellBookButton'>If-taikasana</button>")
			.click(function() { _this.showLesson("ifLessonText"); })
			.hide();
			
			this.ifElseButton = $("<button class='spellBookButton'>If ja else -taikasanat</button>")
			.click(function() { _this.showLesson("ifElseLessonText"); })
			.hide();

			this.comparatorsButton = $("<button class='spellBookButton'>Vertailutapoja</button>")
			.click(function() { _this.showLesson("comparatorsLessonText"); })
			.hide();

			this.listsButton = $("<button class='spellBookButton'>Listat</button>")
			.click(function() { _this.showLesson("listsLessonText"); })
			.hide();

			this.whileButton = $("<button class='spellBookButton'>While-taikasana</button>")
			.click(function() { _this.showLesson("whileLessonText"); })
			.hide();

			this.functionButton = $("<button class='spellBookButton'>Function-taikasana</button>")
			.click(function() { _this.showLesson("functionLessonText"); })
			.hide();

			$("#codeHelpButtons")
			.append(this.codeButton)
			.append(this.variableButton)
			.append(this.stringButton)
			.append(this.intButton)
			.append(this.floatButton)
			.append(this.operatorsButton)
			.append(this.ifButton)
			.append(this.ifElseButton)
			.append(this.comparatorsButton)
			.append(this.whileButton)
			.append(this.listsButton)
			.append(this.functionButton);
		}

		this.lineHintClicked = function() {
			this.lineHint = $("#lineHint").html();
			this.hintButton.click();
		}

		/*
		this.highlightSpellBookLessons = function() {
			var level = _this.levelCounter + 1;

			// Reset background color
			$("#codeHelpButtons button").css("background", "");

			// Set button highlighting and create level specific lessons
			if (level >= 1) {
				if (level == 1) {
					this.codeButton.css("background", "yellow");
					this.variableButton.css("background", "yellow");
					this.stringButton.css("background", "yellow");
				}
			
				var codeLesson = $("#codeLessonText");
				var variableLesson = $("#variableLessonText");
				var stringLesson = $("#stringLessonText");

				this.createLesson(codeLesson);
				this.createLesson(variableLesson);
				this.createLesson(stringLesson);
			

				this.codeButton.show();
				this.variableButton.show();
				this.stringButton.show();
			}
			if (level >= 2) {
				if (level == 2) {
					this.floatButton.css("background", "yellow");
					this.intButton.css("background", "yellow");
				}

				var intLesson = $("#intLessonText");
				var floatLesson = $("#floatLessonText");
				
				this.createLesson(intLesson);
				this.createLesson(floatLesson);

				this.floatButton.show();
				this.intButton.show();
			}
			if (level >= 3) {
				if (level == 3) {
					this.operatorsButton.css("background", "yellow");
				}
				
				var operatorsLesson = $("#operatorsLessonText");
				this.createLesson(operatorsLesson);
				
				this.operatorsButton.show();
			}
			if (level >= 4) {
				if (level == 4) {
					this.ifButton.css("background", "yellow");
					this.ifElseButton.css("background", "yellow");
					this.comparatorsButton.css("background", "yellow");
				}
			
				var ifLesson = $("#ifLessonText");
				var ifElseLesson = $("#ifElseLessonText");
				var comparatorsLesson = $("#comparatorsLessonText");
				
				this.createLesson(ifLesson);
				this.createLesson(ifElseLesson);
				this.createLesson(comparatorsLesson);

				this.ifButton.show();
				this.ifElseButton.show();
				this.comparatorsButton.show();
			}
			if (level >= 5) {
				if (level == 5) {
					this.whileButton.css("background", "yellow");
				}
				
				var whileLesson = $("#whileLessonText");
				this.createLesson(whileLesson);

				this.whileButton.show();
			}
			if (level >= 6) {
				if (level == 6) {
					this.listsButton.css("background", "yellow");
				}
			
				var listsLesson = $("#listsLessonText");
				this.createLesson(listsLesson);
			
				this.listsButton.show();
			}
			if (level >= 7) {
				if (level == 7) {
					this.functionButton.css("background", "yellow");
				}

				var functionLesson = $("#functionLessonText");
				this.createLesson(functionLesson);

				this.functionButton.show();
			}
		}
		*/

		this.playSpeech = function(target) {

			// Any more speeches left?
			if (_this.speechQueue.length == 0) {

				_this.removeDialog();

				$(".executeButton").prop('disabled', false);
				return;
			}

			// Otherwise go to the next speech queue part
			var speech = _this.speechQueue.splice(0, 1)[0];
			_this.createSpeech(speech.character, speech.text, speech.callback);
		}

		// Add a speech to a queue to be shown in a sequence
		this.addSpeech = function(character, text, callback) {
			this.speechQueue.push({character: character, text: text, callback: callback});
		}

		this.createSpeech = function(character, text, callback) {

			// If there is an override going on, put coming speeches to the queue
			if (this.sayOverride == true) {
				this.addSpeech(character, text)
				return;
			}

			$(".executeButton").prop('disabled', true);

			this.removeDialog();
			
			//var speech = document.createElement("div");
			var speech = $("<div></div>");

			// Character may be given as plain string
			if (typeof character == "string")
				var speechText = "<b>" + character + "</b>" + ": " + text;
			else
				var speechText = "<b>" + character.name + "</b>" + ": " + text;

			speech
			.html(speechText)
			.addClass("bubble")
			.css("background", character.color)
			.hide();

			$("#dialogLayer").append(speech);
			speech.show("fade");

			if (callback != undefined)
				callback();
		}

		this.removeDialog = function() {
			$(".bubble").remove();
		}

		this.clearSpeechQueue = function() {
			this.speechQueue = [];
			this.removeDialog();
		}

		this.speechOn = function() {
			if ($(".bubble").length > 0)
				return true;
			return false;
		}

		this.changeLevel = function(level) {
			this.toggleFade("black", "fast", function() {
			
				_this.clearSpeechQueue();

				$("#codeHelp").hide();
				$("#lineHint").hide();
				$("#startTextContainer").hide();
				$("#endTextContainer").hide();

				//_this.player.deBindKeys();
				_this.player.keyDownCallback = undefined;
				_this.player.keyUpCallback = undefined;
				
				_this.gameContext.clearRect(0, 0, _this.gameCanvas.width, _this.gameCanvas.height);
				_this.mapContext.clearRect(0, 0, _this.mapCanvas.width, _this.mapCanvas.height);
				_this.drawContext.clearRect(0, 0, _this.drawCanvas.width, _this.drawCanvas.height);
				_this.animationsController.animations = [];

				_this.level = new level(_this, SpikeTrap, EndObject, TriggerTile, BlockObject);
				_this.mapDrawer.reset();
				_this.mapDrawer.drawMap(_this.level.map, _this.mapContext, _this.tileset, 0);

				_this.level.drawObjects(_this);
				_this.miniQuestsController.levelCheck(_this.levelCounter + 1);
				_this.player.setLevel(_this.level);

				_this.displayStartText();

				_this.level.ended = false;
				_this.levelEnded = false;
			});
		}

		this.endLevel = function(ender) {

			// Don't end level twice to prevent interfering with the level counter
			if (this.level.ended == false) {

				var undone = this.miniQuestsController.levelScrollsUndone(this.levelCounter + 1);

				if (undone == true) {
					this.clearSpeechQueue();
					sano("En ole vielä kerännyt kaikkia kadonneita sivuja.");
					return false;
				}

				this.level.ended = true;

				this.player.setAllowMove(false);

				this.levelEnded = true;
				this.levelCounter += 1;
				this.displayEndText();

				return true;
			}

			return false;
		}

		this.endGame = function() {
			 initMenu(true, this.characterGender);
		}

		// Set a callback which is run every frame
		this.setCallback = function(callback, id) {
			this.callbacks[id] = callback;
		}

		this.clearCallback = function(id) {
			delete this.callbacks[id];
		}

		this.init();
	}

	function Tile(game, options) {
		this.init = function() {
			this.game = game;
			this.context = game.mapContext;

			this.image = new Image();
			this.image.src = options.src;

			this.x = options.x;
			this.y = options.y;

			this.tileX = options.tileX;
			this.tileY = options.tileY;

			this.endable = options.endable;

			this.visible = options.visible;

			this.render();
		}

		this.init();
	}

	Tile.prototype.setVisible = function(visible) {
		this.visible = visible;
		this.render();
	}

	Tile.prototype.render = function() {
		if (this.visible == true) {

			this.context.drawImage(
		 		this.image,
				this.tileX * 32,
				this.tileY * 32,
				32,
				32,
				this.x * 32,
				this.y * 32,
				32,
				32
			);
		}
	};

	// Overriden function to run when players walks on this tile
	Tile.prototype.playerWalk = function() {}

	function SpikeTrap(game, options) {
		Tile.call(this, game, options);

		this.playerWalk = function(player) {
			player.kill();
			return false;
		}
	}
	SpikeTrap.prototype = Object.create(Tile.prototype);

	function TriggerTile(game, options) {
		Tile.call(this, game, options);

		this.triggerInterval = 500;
		this.lastTrigger = Date.now();

		this.visible = options.visible;
		this.triggerCallback = options.triggerCallback;

		this.playerWalk = function(player) {
			// Try controlling spam-like triggering with time intervals
			now = Date.now();

			if (now - this.lastTrigger >= this.triggerInterval) {
				this.lastTrigger = now;
				this.triggerCallback(player);
			}
		}
	}
	TriggerTile.prototype = Object.create(Tile.prototype);

	function EndObject(game, options) {
		this.init = function() {
			this.noEndText = options.noEndText;

			if (this.noEndText == "")
				this.noEndText = undefined

			// Tile may be walkable but can be non-reacting
			if (options.reacting == undefined)
				this.reacting = true;
			else
				this.reacting = options.reacting;

			this.ended = false;

			Tile.call(this, game, options);
		}

		this.setEndable = function(setValue) {
			this.endable = setValue;
		}

		this.playerWalk = function(player) {
			if (this.reacting == false)
				return true;

			if (this.ended == false) {
				if (this.endable == true) {
					if (this.game.endLevel() == true)
						this.ended = true;
				}
				else if (this.noEndText != undefined) {
					var pos = this.game.player.getPosition();
					this.game.createSpeech(this.game.player, this.noEndText);
				}
			}

			return false;
		}
		this.init()
	}
	EndObject.prototype = Object.create(Tile.prototype);
	EndObject.prototype.constructor = EndObject;

	// This object blocks player movement
	function BlockObject(game, options) {
		this.init = function() {
			this.blocking = options.blocking;

			Tile.call(this, game, options);
		}

		this.playerWalk = function(player) {
			if (this.blocking == true)
				return false;
			return true;
		}

		this.init();
	}
	BlockObject.prototype = Object.create(Tile.prototype);

	function NpcBlock(npc, options) {
		var _this = this;

		this.init = function() {
			Tile.call(this, npc, options);
			this.npc = npc;
		}

		this.playerWalk = function(player) {
			// Lower block's hitbox a bit
			var pos = player.getPosition();
			var y = pos.y;

			var npcPos = _this.npc.getPosition();
			var npcY = npcPos.y;

			if (npcY - y <= 1.7) {
				this.npc.talk();
				return false;
			}

			return true;
		}

		this.init()
	}
	NpcBlock.prototype = Object.create(Tile.prototype);
	NpcBlock.prototype.constructor = NpcBlock;

	function Animation(options) {
		// Create sprite sheet
		this.image = new Image();	
		this.image.src = options.src;

		this.frameIndex = 0;
		this.tickCount = 0;0
		this.ticksPerFrame = options.ticksPerFrame || 0;
		this.numberOfFrames = options.numberOfFrames || 1;
		this.originalNumberOfFrames = this.numberOfFrames;
		
		this.canvas = options.canvas;

		this.context = this.canvas.getContext("2d");
		this.width = options.width;
		this.height = options.height;
		this.x = options.x;
		this.y = options.y;
		this.frameWidth = options.frameWidth;
		this.frameHeight = options.frameHeight;
		this.animationLine = options.animationLine;

		this.animationStopped = false;

		this.setAnimationLine = function(facing) {
			this.animationLine = facing;
		}

		this.stopAnimation = function() {
			this.numberOfFrames = 0;
			this.frameIndex = 0;
			this.animationStopped = true;
		}

		// Don't start animation right away
		this.stopAnimation();

		this.startAnimation = function() {
			this.numberOfFrames = this.originalNumberOfFrames;
			this.animationStopped = false;
		}

		this.update = function () {
			if (this.animationStopped == true)
				return;

            this.tickCount += 1;

            if (this.tickCount > this.ticksPerFrame) {
				this.tickCount = 0;
				
                // If the current frame index is in range
                if (this.frameIndex < this.numberOfFrames - 1)
                    this.frameIndex += 1;
                else
                    this.frameIndex = 0;
		    }
		};

		this.changeImage = function(src) {
			this.image = new Image();
			this.image.src = src;
		}

		this.render = function () {
			// Draw the animation
			this.context.drawImage(
		 		this.image,
				this.frameIndex * this.frameWidth,
				this.animationLine * this.frameHeight,
				this.frameWidth,
				this.frameHeight,
				this.x,
				this.y,
				this.frameWidth,
				this.frameHeight
			);
		};
	}

	function AnimationsController(game) {

		this.game = game;
		this.animations = [];

		this.removeAnimation = function(removed) {
			for (i in this.animations) {
				var animation = this.animations[i];
				
				if (removed === animation)
					this.animations.splice(i, 1);
			}
		}

		this.newAnimation = function(options) {

			// Create sprite
			animation = new Animation({
				canvas: this.game.gameCanvas,
				width: options.width,
				height: options.height,
				src: options.src,
				numberOfFrames: options.frames,
				ticksPerFrame: 4,
				frameWidth: options.frameWidth,
				frameHeight: options.frameHeight,
				animationLine: options.animationLine,
				x: options.x,
				y: options.y
			});

			this.animations.push(animation)

			return animation;
		}
	}

	function initMenu(gameEnd, gender) {
		var characterGender;

		$("#mainMenu").show("fade");

		if (gameEnd == true) {
			$("#endGame").show();
			$("#selectMale").hide();
			$("#selectFemale").hide();

			if (gender == "male")
				$("#endGameMale").show();
			else
				$("#endGameFemale").show();

			return;
		}

		$("#selectMale").click(function() {
			characterGender = "male";
			$("#levelChooser").css("display", "block");
		});

		$("#selectFemale").click(function() {
			characterGender = "female";
			$("#levelChooser").css("display", "block");
		});

		$("#startLevel1").click(function() { createGame(1, characterGender); });
		$("#startLevel2").click(function() { createGame(2, characterGender); });
		$("#startLevel3").click(function() { createGame(3, characterGender); });
		$("#startLevel4").click(function() { createGame(4, characterGender); });
		$("#startLevel5").click(function() { createGame(5, characterGender); });
		$("#startLevel6").click(function() { createGame(6, characterGender); });
		$("#startLevel7").click(function() { createGame(7, characterGender); });
	}

	function createGame(level, gender) {
		game = new Game(level, gender);
		$("#mainMenu").hide("fade"); 
	}


	window.addEventListener("load", function() {
		initMenu(false);
		//game = new Game(7, "male"); $("#mainMenu").hide();
	})
} ());